# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the HashiCorpVault System. The API that was used to build the adapter for HashiCorpVault is usually available in the report directory of this adapter. The adapter utilizes the HashiCorpVault API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The HashiCorp Vault adapter from Itential is used to integrate the Itential Automation Platform (IAP) with HashiCorp Vault. With this adapter you have the ability to perform operations on items such as:

- Secrets
- Identity

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
