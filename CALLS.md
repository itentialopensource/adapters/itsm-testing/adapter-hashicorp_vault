## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for HashiCorp Vault. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for HashiCorp Vault.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the HashiCorpVault. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getKvSecretConfig(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the current configuration for the secrets backend at the given path</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretConfig(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Configure backend level settings that are applied to every key in the key-value store.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKvSecretDataPath(namespace, secretMount, pathParam, callback)</td>
    <td style="padding:15px">Retrieves the secret at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretDataPath(namespace, secretMount, pathParam, body, callback)</td>
    <td style="padding:15px">Creates a new version of a secret at the specified location</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKvSecretDataPath(namespace, secretMount, pathParam, callback)</td>
    <td style="padding:15px">Issues a soft delete of the secret's latest version at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretDeletePath(namespace, secretMount, pathParam, body, callback)</td>
    <td style="padding:15px">Issues a soft delete of the specified versions of the secret</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/delete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretDestroyPath(namespace, secretMount, pathParam, body, callback)</td>
    <td style="padding:15px">Permanently removes the specified version data for the provided key and version numbers from the secret store.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/destroy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKvSecretMetadataPath(namespace, secretMount, pathParam, list, callback)</td>
    <td style="padding:15px">Retrieves the metadata and versions for the secret at the specified path.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretMetadataPath(namespace, secretMount, pathParam, body, callback)</td>
    <td style="padding:15px">Creates or updates the metadata of a secret at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKvSecretMetadataPath(namespace, secretMount, pathParam, callback)</td>
    <td style="padding:15px">Permanently deletes the key metadata and all version data for the specified key.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvSecretUndeletePath(namespace, secretMount, pathParam, body, callback)</td>
    <td style="padding:15px">Undeletes the data for the provided version and path in the secret store.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/undelete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCubbyholeSecretPath(namespace, secretMount, pathParam, list, callback)</td>
    <td style="padding:15px">Retrieves the secret at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCubbyholeSecretPath(namespace, secretMount, pathParam, callback)</td>
    <td style="padding:15px">Store a secret at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCubbyholeSecretPath(namespace, secretMount, pathParam, callback)</td>
    <td style="padding:15px">Deletes the secret at the specified location.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCa(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the CA certificate in raw DER-encoded form</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCaPem(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the CA certificate in raw PEM form</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCaChain(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the CA certificate chain, including the CA in PEM format.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCertCaChain(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the CA certificate chain, including the CA in PEM format.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCertCrl(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves one of a selection of certificates.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCertSerial(namespace, secretMount, serial, callback)</td>
    <td style="padding:15px">Retrieves one of a selection of certificates.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCerts(namespace, secretMount, list, callback)</td>
    <td style="padding:15px">Returns the current certificates</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/certs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretConfigCa(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Set the CA information for the backend via a PEM file containing the CA certificate and its private key, concatenated.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretConfigCrl(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the duration for which the generated CRL should be marked valid.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretConfigCrl(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Sets the duration for which the generated CRL should be marked valid.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretConfigUrls(namespace, secretMount, callback)</td>
    <td style="padding:15px">Fetches the URLs to be encoded in generated certificates.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretConfigUrls(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Sets the issuing certificate endpoints, CRL distribution points, and OCSP server endpoints that will be encoded into issued certificates.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCrl(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the current CRL in raw DER-encoded form.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCrlPem(namespace, secretMount, callback)</td>
    <td style="padding:15px">Retrieves the current CRL in raw PEM form.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretCrlRotate(namespace, secretMount, callback)</td>
    <td style="padding:15px">Forces a rotation of the CRL.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretGenerateIntermediate(namespace, secretMount, exported, body, callback)</td>
    <td style="padding:15px">Generates a new private key and a CSR for signing.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretSetSignedIntermediate(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Allows submitting the signed CA certificate corresponding to a private key generated via /pki/intermediate/generate.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/set-signed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretIssueCert(namespace, secretMount, role, body, callback)</td>
    <td style="padding:15px">Generates a new set of credentials (private key and certificate) based on the role named in the endpoint.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/issue/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretRevokeCert(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Revoke a certificate by serial number.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPkiSecretRoles(namespace, secretMount, list, callback)</td>
    <td style="padding:15px">List the existing roles in this backend</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiSecretRolesName(namespace, secretMount, name, callback)</td>
    <td style="padding:15px">Queries the role definition.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretRolesName(namespace, secretMount, name, body, callback)</td>
    <td style="padding:15px">Creates or updates the role definition.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePkiSecretRolesName(namespace, secretMount, name, callback)</td>
    <td style="padding:15px">Deletes the role definition.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePkiSecretRoot(namespace, secretMount, callback)</td>
    <td style="padding:15px">Deletes the root CA key to allow a new one to be generated.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretGenerateRoot(namespace, secretMount, exported, body, callback)</td>
    <td style="padding:15px">Generates a new self-signed CA certificate and private key.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretSignIntermediateRoot(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Issue an intermediate CA certificate based on the provided CSR.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-intermediate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretRootSignSelfIssued(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Signs another CA's self-issued certificate.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-self-issued?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretSignVerbatim(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Signs a new certificate based upon the provided CSR.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretSignVerbatimName(namespace, secretMount, role, body, callback)</td>
    <td style="padding:15px">Signs a new certificate based upon the provided CSR. Values are taken verbatim from the CSR.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretSignRole(namespace, secretMount, role, body, callback)</td>
    <td style="padding:15px">Sign certificates using a certain role with the provided details.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSecretTidy(namespace, secretMount, body, callback)</td>
    <td style="padding:15px">Tidy up the backend by removing expired certificates, revocation information,
or both.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tidy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleLogin(body, callback)</td>
    <td style="padding:15px">Issue a token based on the credentials supplied</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRole(list, callback)</td>
    <td style="padding:15px">Lists all the roles registered with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleName(roleName, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleName(roleName, body, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleName(roleName, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameBindSecretId(roleName, callback)</td>
    <td style="padding:15px">Impose secret_id to be presented during login using this role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bind-secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameBindSecretId(roleName, body, callback)</td>
    <td style="padding:15px">Impose secret_id to be presented during login using this role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bind-secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameBindSecretId(roleName, callback)</td>
    <td style="padding:15px">Impose secret_id to be presented during login using this role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bind-secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameBoundCidrList(roleName, callback)</td>
    <td style="padding:15px">Deprecated: Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bound-cidr-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameBoundCidrList(roleName, body, callback)</td>
    <td style="padding:15px">Deprecated: Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bound-cidr-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameBoundCidrList(roleName, callback)</td>
    <td style="padding:15px">Deprecated: Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/bound-cidr-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameCustomSecretId(roleName, body, callback)</td>
    <td style="padding:15px">Assign a SecretID of choice against the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/custom-secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameLocalSecretIds(roleName, callback)</td>
    <td style="padding:15px">Enables cluster local secret IDs</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/local-secret-ids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNamePeriod(roleName, callback)</td>
    <td style="padding:15px">Updates the value of 'period' on the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/period?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNamePeriod(roleName, body, callback)</td>
    <td style="padding:15px">Updates the value of 'period' on the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/period?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNamePeriod(roleName, callback)</td>
    <td style="padding:15px">Updates the value of 'period' on the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/period?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNamePolicies(roleName, callback)</td>
    <td style="padding:15px">Policies of the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNamePolicies(roleName, body, callback)</td>
    <td style="padding:15px">Policies of the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNamePolicies(roleName, callback)</td>
    <td style="padding:15px">Policies of the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameRoleId(roleName, callback)</td>
    <td style="padding:15px">Returns the 'role_id' of the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/role-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameRoleId(roleName, body, callback)</td>
    <td style="padding:15px">Returns the 'role_id' of the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/role-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameSecretId(roleName, list, callback)</td>
    <td style="padding:15px">Generate a SecretID against this role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretId(roleName, body, callback)</td>
    <td style="padding:15px">Generate a SecretID against this role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdAccessorDestroy(roleName, body, callback)</td>
    <td style="padding:15px">postAuthApproleRoleRole_nameSecretIdAccessorDestroy</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-accessor/destroy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy(roleName, callback)</td>
    <td style="padding:15px">deleteAuthApproleRoleRole_nameSecretIdAccessorDestroy</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-accessor/destroy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdAccessorLookup(roleName, body, callback)</td>
    <td style="padding:15px">postAuthApproleRoleRole_nameSecretIdAccessorLookup</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-accessor/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameSecretIdBoundCidrs(roleName, callback)</td>
    <td style="padding:15px">Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can perform the</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdBoundCidrs(roleName, body, callback)</td>
    <td style="padding:15px">Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can perform the</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameSecretIdBoundCidrs(roleName, callback)</td>
    <td style="padding:15px">Comma separated list of CIDR blocks, if set, specifies blocks of IP
addresses which can perform the</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameSecretIdNumUses(roleName, callback)</td>
    <td style="padding:15px">Use limit of the SecretID generated against the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdNumUses(roleName, body, callback)</td>
    <td style="padding:15px">Use limit of the SecretID generated against the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameSecretIdNumUses(roleName, callback)</td>
    <td style="padding:15px">Use limit of the SecretID generated against the role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameSecretIdTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, representing the lifetime of the SecretIDs
that are generated against the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdTtl(roleName, body, callback)</td>
    <td style="padding:15px">Duration in seconds, representing the lifetime of the SecretIDs
that are generated against the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameSecretIdTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, representing the lifetime of the SecretIDs
that are generated against the role</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdDestroy(roleName, body, callback)</td>
    <td style="padding:15px">Invalidate an issued secret_id</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id/destroy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameSecretIdDestroy(roleName, callback)</td>
    <td style="padding:15px">Invalidate an issued secret_id</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id/destroy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameSecretIdLookup(roleName, body, callback)</td>
    <td style="padding:15px">Read the properties of an issued secret_id</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/secret-id/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameTokenBoundCidrs(roleName, callback)</td>
    <td style="padding:15px">Comma separated string or list of CIDR blocks. If set, specifies the blocks of
IP addresses which c</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameTokenBoundCidrs(roleName, body, callback)</td>
    <td style="padding:15px">Comma separated string or list of CIDR blocks. If set, specifies the blocks of
IP addresses which c</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameTokenBoundCidrs(roleName, callback)</td>
    <td style="padding:15px">Comma separated string or list of CIDR blocks. If set, specifies the blocks of
IP addresses which c</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-bound-cidrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameTokenMaxTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, the maximum lifetime of the tokens issued by using
the SecretIDs that were gen</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-max-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameTokenMaxTtl(roleName, body, callback)</td>
    <td style="padding:15px">Duration in seconds, the maximum lifetime of the tokens issued by using
the SecretIDs that were gen</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-max-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameTokenMaxTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, the maximum lifetime of the tokens issued by using
the SecretIDs that were gen</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-max-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameTokenNumUses(roleName, callback)</td>
    <td style="padding:15px">Number of times issued tokens can be used</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameTokenNumUses(roleName, body, callback)</td>
    <td style="padding:15px">Number of times issued tokens can be used</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameTokenNumUses(roleName, callback)</td>
    <td style="padding:15px">Number of times issued tokens can be used</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-num-uses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthApproleRoleRoleNameTokenTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, the lifetime of the token issued by using the SecretID that
is generated again</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleRoleRoleNameTokenTtl(roleName, body, callback)</td>
    <td style="padding:15px">Duration in seconds, the lifetime of the token issued by using the SecretID that
is generated again</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthApproleRoleRoleNameTokenTtl(roleName, callback)</td>
    <td style="padding:15px">Duration in seconds, the lifetime of the token issued by using the SecretID that
is generated again</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/role/{pathv1}/token-ttl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthApproleTidySecretId(callback)</td>
    <td style="padding:15px">Trigger the clean-up of expired SecretID entries.</td>
    <td style="padding:15px">{base_path}/{version}/auth/approle/tidy/secret-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapConfig(callback)</td>
    <td style="padding:15px">Configure the LDAP server to connect to, along with its options.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapConfig(body, callback)</td>
    <td style="padding:15px">Configure the LDAP server to connect to, along with its options.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapDuoAccess(body, callback)</td>
    <td style="padding:15px">Configure the access keys and host for Duo API connections.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/duo/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapDuoConfig(callback)</td>
    <td style="padding:15px">Configure Duo second factor behavior.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/duo/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapDuoConfig(body, callback)</td>
    <td style="padding:15px">Configure Duo second factor behavior.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/duo/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapGroups(list, callback)</td>
    <td style="padding:15px">Manage additional groups for users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapGroupsName(name, callback)</td>
    <td style="padding:15px">Manage additional groups for users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapGroupsName(name, body, callback)</td>
    <td style="padding:15px">Manage additional groups for users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthLdapGroupsName(name, callback)</td>
    <td style="padding:15px">Manage additional groups for users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapLoginUsername(username, body, callback)</td>
    <td style="padding:15px">Log in with a username and password.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/login/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapMfaConfig(callback)</td>
    <td style="padding:15px">Configure multi factor backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/mfa_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapMfaConfig(body, callback)</td>
    <td style="padding:15px">Configure multi factor backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/mfa_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapUsers(list, callback)</td>
    <td style="padding:15px">Manage users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthLdapUsersName(name, callback)</td>
    <td style="padding:15px">Manage users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLdapUsersName(name, body, callback)</td>
    <td style="padding:15px">Manage users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthLdapUsersName(name, callback)</td>
    <td style="padding:15px">Manage users allowed to authenticate.</td>
    <td style="padding:15px">{base_path}/{version}/auth/ldap/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthRancherConfig(callback)</td>
    <td style="padding:15px">Configures the JWT Public Key and Kubernetes API information.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthRancherConfig(body, callback)</td>
    <td style="padding:15px">Configures the JWT Public Key and Kubernetes API information.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthRancherLogin(body, callback)</td>
    <td style="padding:15px">Authenticates Kubernetes service accounts with Vault.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthRancherRole(list, callback)</td>
    <td style="padding:15px">Lists all the roles registered with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthRancherRoleName(name, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthRancherRoleName(name, body, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthRancherRoleName(name, callback)</td>
    <td style="padding:15px">Register an role with the backend.</td>
    <td style="padding:15px">{base_path}/{version}/auth/rancher/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenAccessors(list, callback)</td>
    <td style="padding:15px">List token accessors, which can then be
be used to iterate and discover their properties
or revoke</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/accessors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenCreateWithNamespaceHeader(namespace, callback)</td>
    <td style="padding:15px">The token create path is used to create new token with specified X-Vault-Namespace header.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenCreateOrphan(callback)</td>
    <td style="padding:15px">The token create path is used to create new orphan tokens.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/create-orphan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenCreateRoleName(roleName, callback)</td>
    <td style="padding:15px">This token create path is used to create new tokens adhering to the given role.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/create/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenLookup(callback)</td>
    <td style="padding:15px">This endpoint will lookup a token and its properties.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenLookup(body, callback)</td>
    <td style="padding:15px">This endpoint will lookup a token and its properties.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenLookupAccessor(body, callback)</td>
    <td style="padding:15px">This endpoint will lookup a token associated with the given accessor and its properties. Response w</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/lookup-accessor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenLookupSelf(callback)</td>
    <td style="padding:15px">This endpoint will lookup a token and its properties.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/lookup-self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenLookupSelf(body, callback)</td>
    <td style="padding:15px">This endpoint will lookup a token and its properties.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/lookup-self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRenew(body, callback)</td>
    <td style="padding:15px">This endpoint will renew the given token and prevent expiration.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRenewAccessor(body, callback)</td>
    <td style="padding:15px">This endpoint will renew a token associated with the given accessor and its properties. Response wi</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/renew-accessor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRenewSelf(body, callback)</td>
    <td style="padding:15px">This endpoint will renew the token used to call it and prevent expiration.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/renew-self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRevoke(body, callback)</td>
    <td style="padding:15px">This endpoint will delete the given token and all of its child tokens.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRevokeAccessor(body, callback)</td>
    <td style="padding:15px">This endpoint will delete the token associated with the accessor and all of its child tokens.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/revoke-accessor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRevokeOrphan(body, callback)</td>
    <td style="padding:15px">This endpoint will delete the token and orphan its child tokens.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/revoke-orphan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRevokeSelf(callback)</td>
    <td style="padding:15px">This endpoint will delete the token used to call it and all of its child tokens.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/revoke-self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenRoles(list, callback)</td>
    <td style="padding:15px">This endpoint lists configured roles.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthTokenRolesRoleName(roleName, callback)</td>
    <td style="padding:15px">getAuthTokenRolesRole_name</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenRolesRoleName(roleName, body, callback)</td>
    <td style="padding:15px">postAuthTokenRolesRole_name</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthTokenRolesRoleName(roleName, callback)</td>
    <td style="padding:15px">deleteAuthTokenRolesRole_name</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthTokenTidy(callback)</td>
    <td style="padding:15px">This endpoint performs cleanup tasks that can be run if certain error
conditions have occurred.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token/tidy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityAlias(namespace, body, callback)</td>
    <td style="padding:15px">Create a new alias.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/alias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityAliases(namespace, list, callback)</td>
    <td style="padding:15px">List all the alias IDs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/alias/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Get Identity alias By ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityAliasById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update Identity alias By ID</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Delete Identity alias By ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityEntity(namespace, body, callback)</td>
    <td style="padding:15px">Create a new entity</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityEntityAlias(namespace, body, callback)</td>
    <td style="padding:15px">Create a new entity alias.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity-alias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntityAliases(namespace, list, callback)</td>
    <td style="padding:15px">List all the entity alias IDs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity-alias/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntityAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Get Identity entity alias by ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityEntityAliasById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update Identity entity alias by ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityEntityAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Delete Identity entity alias by ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityEntityBatchDelete(namespace, body, callback)</td>
    <td style="padding:15px">Delete all of the entities provided</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/batch-delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntities(namespace, list, callback)</td>
    <td style="padding:15px">List all the entity IDs</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntityById(namespace, id, callback)</td>
    <td style="padding:15px">Get Identity entity using entity ID</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityEntityById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update Identity entity using entity ID</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityEntityById(namespace, id, callback)</td>
    <td style="padding:15px">Delete Identity entity using entity ID</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityEntityMerge(namespace, body, callback)</td>
    <td style="padding:15px">Merge two or more entities together</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntityNames(namespace, list, callback)</td>
    <td style="padding:15px">List all the entity names</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityEntityByName(namespace, name, callback)</td>
    <td style="padding:15px">Get Identity entity using entity name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityEntityByName(namespace, name, body, callback)</td>
    <td style="padding:15px">Update Identity entity using entity name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityEntityByName(namespace, name, callback)</td>
    <td style="padding:15px">Delete Identity entity using entity name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/entity/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityGroup(namespace, body, callback)</td>
    <td style="padding:15px">Create a new group.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityGroupAlias(namespace, body, callback)</td>
    <td style="padding:15px">Creates a new group alias, or updates an existing one.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group-alias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroupAliasId(namespace, list, callback)</td>
    <td style="padding:15px">List all the group alias IDs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group-alias/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroupAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Get Identity Group Alias By Id</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityGroupAliasById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update Identity Group Alias By Id</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityGroupAliasById(namespace, id, callback)</td>
    <td style="padding:15px">Delete Identity Group Alias By Id</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group-alias/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroups(namespace, list, callback)</td>
    <td style="padding:15px">List all the group IDs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroupById(namespace, id, callback)</td>
    <td style="padding:15px">Get existing group using its ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityGroupById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update an existing group using its ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityGroupById(namespace, id, callback)</td>
    <td style="padding:15px">Delete an existing group using its ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroupNames(namespace, list, callback)</td>
    <td style="padding:15px">Get Identity Group Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroupByName(namespace, name, callback)</td>
    <td style="padding:15px">Get Identity Group By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityGroupByName(namespace, name, body, callback)</td>
    <td style="padding:15px">Update Identity Group By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityGroupByName(namespace, name, callback)</td>
    <td style="padding:15px">Delete Identity Group By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/group/name/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityLookupEntity(namespace, body, callback)</td>
    <td style="padding:15px">Query entities based on various properties.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/lookup/entity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityLookupGroup(namespace, body, callback)</td>
    <td style="padding:15px">Query groups based on various properties.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/lookup/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcWellKnownKeys(namespace, callback)</td>
    <td style="padding:15px">Retrieve public keys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/.well-known/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcWellKnownOpenidConfiguration(namespace, callback)</td>
    <td style="padding:15px">Query OIDC configurations</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/.well-known/openid-configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcConfig(namespace, callback)</td>
    <td style="padding:15px">OIDC configuration</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityOidcConfig(namespace, body, callback)</td>
    <td style="padding:15px">OIDC configuration</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityOidcIntrospect(namespace, body, callback)</td>
    <td style="padding:15px">Verify the authenticity of an OIDC token</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/introspect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcKeys(namespace, list, callback)</td>
    <td style="padding:15px">List OIDC keys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcKeyByName(namespace, name, callback)</td>
    <td style="padding:15px">CRUD operations for OIDC keys.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/key/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityOidcKeyByName(namespace, name, body, callback)</td>
    <td style="padding:15px">Update for OIDC keys.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/key/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityOidcKeyByName(namespace, name, callback)</td>
    <td style="padding:15px">Delete for OIDC keys.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/key/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityOidcKeyRotateByName(namespace, name, body, callback)</td>
    <td style="padding:15px">Rotate a named OIDC key.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/key/{pathv2}/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcRoles(namespace, list, callback)</td>
    <td style="padding:15px">List configured OIDC roles</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcRoleByName(namespace, name, callback)</td>
    <td style="padding:15px">Get OIDC Roles By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityOidcRoleByName(namespace, name, body, callback)</td>
    <td style="padding:15px">Update OIDC Roles By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityOidcRoleByName(namespace, name, callback)</td>
    <td style="padding:15px">Delete OIDC Roles By Name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityOidcTokenByName(namespace, name, callback)</td>
    <td style="padding:15px">Generate an OIDC token</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/oidc/token/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIdentityPersona(namespace, body, callback)</td>
    <td style="padding:15px">Create a new alias.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/persona?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityPersonas(namespace, list, callback)</td>
    <td style="padding:15px">List all the alias IDs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/persona/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityPersonaById(namespace, id, callback)</td>
    <td style="padding:15px">Get Identity Persona By ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/persona/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIdentityPersonaById(namespace, id, body, callback)</td>
    <td style="padding:15px">Update Identity Persona By ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/persona/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIdentityPersonaById(namespace, id, callback)</td>
    <td style="padding:15px">Delete Identity Persona By ID.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/identity/persona/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysAudit(callback)</td>
    <td style="padding:15px">List the enabled audit devices.</td>
    <td style="padding:15px">{base_path}/{version}/sys/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysAuditHashPath(pathParam, body, callback)</td>
    <td style="padding:15px">The hash of the given string via the given audit backend</td>
    <td style="padding:15px">{base_path}/{version}/sys/audit-hash/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysAuditPath(pathParam, body, callback)</td>
    <td style="padding:15px">Enable a new audit device at the supplied path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/audit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysAuditPath(pathParam, callback)</td>
    <td style="padding:15px">Disable the audit device at the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/audit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysAuth(callback)</td>
    <td style="padding:15px">List the currently enabled credential backends.</td>
    <td style="padding:15px">{base_path}/{version}/sys/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysAuthPath(pathParam, body, callback)</td>
    <td style="padding:15px">Enables a new auth method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysAuthPath(pathParam, callback)</td>
    <td style="padding:15px">Disable the auth method at the given auth path</td>
    <td style="padding:15px">{base_path}/{version}/sys/auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysAuthPathTune(pathParam, callback)</td>
    <td style="padding:15px">Reads the given auth path's configuration.</td>
    <td style="padding:15px">{base_path}/{version}/sys/auth/{pathv1}/tune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysAuthPathTune(pathParam, body, callback)</td>
    <td style="padding:15px">Tune configuration parameters for a given auth path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/auth/{pathv1}/tune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysCapabilities(body, callback)</td>
    <td style="padding:15px">Fetches the capabilities of the given token on the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysCapabilitiesAccessor(body, callback)</td>
    <td style="padding:15px">Fetches the capabilities of the token associated with the given token, on the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/capabilities-accessor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysCapabilitiesSelf(body, callback)</td>
    <td style="padding:15px">Fetches the capabilities of the given token on the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/capabilities-self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigAuditingRequestHeaders(callback)</td>
    <td style="padding:15px">List the request headers that are configured to be audited.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/auditing/request-headers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigAuditingRequestHeadersHeader(header, callback)</td>
    <td style="padding:15px">List the information for the given request header.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/auditing/request-headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysConfigAuditingRequestHeadersHeader(header, body, callback)</td>
    <td style="padding:15px">Enable auditing of a header.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/auditing/request-headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysConfigAuditingRequestHeadersHeader(header, callback)</td>
    <td style="padding:15px">Disable auditing of the given request header.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/auditing/request-headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigControlGroup(callback)</td>
    <td style="padding:15px">Configure control group global settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/control-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysConfigControlGroup(body, callback)</td>
    <td style="padding:15px">Configure control group global settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/control-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysConfigControlGroup(callback)</td>
    <td style="padding:15px">Configure control group global settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/control-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigCors(callback)</td>
    <td style="padding:15px">Return the current CORS settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/cors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysConfigCors(body, callback)</td>
    <td style="padding:15px">Configure the CORS settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/cors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysConfigCors(callback)</td>
    <td style="padding:15px">Remove any CORS settings.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/cors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigStateSanitized(callback)</td>
    <td style="padding:15px">Return a sanitized version of the Vault server configuration.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/state/sanitized?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigUiHeaders(list, callback)</td>
    <td style="padding:15px">Return a list of configured UI headers.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/ui/headers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysConfigUiHeadersHeader(header, callback)</td>
    <td style="padding:15px">Return the given UI header's configuration</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/ui/headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysConfigUiHeadersHeader(header, body, callback)</td>
    <td style="padding:15px">Configure the values to be returned for the UI header.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/ui/headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysConfigUiHeadersHeader(header, callback)</td>
    <td style="padding:15px">Remove a UI header.</td>
    <td style="padding:15px">{base_path}/{version}/sys/config/ui/headers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysControlGroupAuthorize(body, callback)</td>
    <td style="padding:15px">Authorize a control group request</td>
    <td style="padding:15px">{base_path}/{version}/sys/control-group/authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysControlGroupRequest(body, callback)</td>
    <td style="padding:15px">Check the status of a control group request</td>
    <td style="padding:15px">{base_path}/{version}/sys/control-group/request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysGenerateRoot(callback)</td>
    <td style="padding:15px">Read the configuration and progress of the current root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysGenerateRoot(body, callback)</td>
    <td style="padding:15px">Initializes a new root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysGenerateRoot(callback)</td>
    <td style="padding:15px">Cancels any in-progress root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysGenerateRootAttempt(callback)</td>
    <td style="padding:15px">Read the configuration and progress of the current root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root/attempt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysGenerateRootAttempt(body, callback)</td>
    <td style="padding:15px">Initializes a new root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root/attempt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysGenerateRootAttempt(callback)</td>
    <td style="padding:15px">Cancels any in-progress root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root/attempt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysGenerateRootUpdate(body, callback)</td>
    <td style="padding:15px">Enter a single master key share to progress the root generation attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/generate-root/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysHealth(callback)</td>
    <td style="padding:15px">Returns the health status of Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysHostInfo(callback)</td>
    <td style="padding:15px">Information about the host instance that this Vault server is running on.</td>
    <td style="padding:15px">{base_path}/{version}/sys/host-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInit(callback)</td>
    <td style="padding:15px">Returns the initialization status of Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysInit(body, callback)</td>
    <td style="padding:15px">Initialize a new Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalSpecsOpenapi(callback)</td>
    <td style="padding:15px">Generate an OpenAPI 3 document of all mounted paths.</td>
    <td style="padding:15px">{base_path}/{version}/sys/internal/specs/openapi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalUiMounts(callback)</td>
    <td style="padding:15px">Lists all enabled and visible auth and secrets mounts.</td>
    <td style="padding:15px">{base_path}/{version}/sys/internal/ui/mounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalUiMountsPath(pathParam, callback)</td>
    <td style="padding:15px">Return information about the given mount.</td>
    <td style="padding:15px">{base_path}/{version}/sys/internal/ui/mounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysKeyStatus(callback)</td>
    <td style="padding:15px">Provides information about the backend encryption key.</td>
    <td style="padding:15px">{base_path}/{version}/sys/key-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysLeader(callback)</td>
    <td style="padding:15px">Returns the high availability status and current leader instance of Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leader?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesLookup(body, callback)</td>
    <td style="padding:15px">Retrieve lease metadata.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysLeasesLookup(list, callback)</td>
    <td style="padding:15px">Returns a list of lease ids.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/lookup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysLeasesLookupPrefix(prefix, list, callback)</td>
    <td style="padding:15px">Returns a list of lease ids.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/lookup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRenew(body, callback)</td>
    <td style="padding:15px">Renews a lease, requesting to extend the lease.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRenewUrlLeaseId(urlLeaseId, body, callback)</td>
    <td style="padding:15px">Renews a lease, requesting to extend the lease.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/renew/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRevoke(body, callback)</td>
    <td style="padding:15px">Revokes a lease immediately.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRevokeForcePrefix(prefix, callback)</td>
    <td style="padding:15px">Revokes all secrets or tokens generated under a given prefix immediately</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/revoke-force/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRevokePrefixPrefix(prefix, body, callback)</td>
    <td style="padding:15px">Revokes all secrets (via a lease ID prefix) or tokens (via the tokens' path property) generated und</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/revoke-prefix/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesRevokeUrlLeaseId(urlLeaseId, body, callback)</td>
    <td style="padding:15px">Revokes a lease immediately.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/revoke/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLeasesTidy(callback)</td>
    <td style="padding:15px">This endpoint performs cleanup tasks that can be run if certain error
conditions have occurred.</td>
    <td style="padding:15px">{base_path}/{version}/sys/leases/tidy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysLicense(callback)</td>
    <td style="padding:15px">The path responds to the following HTTP methods.

    GET /
        Returns information on the inst</td>
    <td style="padding:15px">{base_path}/{version}/sys/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysLicense(body, callback)</td>
    <td style="padding:15px">The path responds to the following HTTP methods.

    GET /
        Returns information on the inst</td>
    <td style="padding:15px">{base_path}/{version}/sys/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMetrics(format, callback)</td>
    <td style="padding:15px">Export the metrics aggregated for telemetry purpose.</td>
    <td style="padding:15px">{base_path}/{version}/sys/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethod(list, callback)</td>
    <td style="padding:15px">Lists all the available MFA methods by their name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethodDuoName(name, callback)</td>
    <td style="padding:15px">Defines or updates a Duo MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/duo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodDuoName(name, body, callback)</td>
    <td style="padding:15px">Defines or updates a Duo MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/duo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysMfaMethodDuoName(name, callback)</td>
    <td style="padding:15px">Defines or updates a Duo MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/duo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethodOktaName(name, callback)</td>
    <td style="padding:15px">Defines or updates an Okta MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/okta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodOktaName(name, body, callback)</td>
    <td style="padding:15px">Defines or updates an Okta MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/okta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysMfaMethodOktaName(name, callback)</td>
    <td style="padding:15px">Defines or updates an Okta MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/okta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethodPingidName(name, callback)</td>
    <td style="padding:15px">Defines or updates a PingID MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/pingid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodPingidName(name, body, callback)</td>
    <td style="padding:15px">Defines or updates a PingID MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/pingid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysMfaMethodPingidName(name, callback)</td>
    <td style="padding:15px">Defines or updates a PingID MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/pingid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethodTotpName(name, callback)</td>
    <td style="padding:15px">Defines or updates a TOTP MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodTotpName(name, body, callback)</td>
    <td style="padding:15px">Defines or updates a TOTP MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysMfaMethodTotpName(name, callback)</td>
    <td style="padding:15px">Defines or updates a TOTP MFA method.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodTotpNameAdminDestroy(name, body, callback)</td>
    <td style="padding:15px">Deletes the TOTP secret for the given method name on the given entity.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}/admin-destroy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMfaMethodTotpNameAdminGenerate(name, body, callback)</td>
    <td style="padding:15px">Generates a TOTP secret for the given method name on the given entity.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}/admin-generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMfaMethodTotpNameGenerate(name, callback)</td>
    <td style="padding:15px">Generates a TOTP secret for the given method name on the entity of the
		calling token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mfa/method/totp/{pathv1}/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMonitor(logLevel, callback)</td>
    <td style="padding:15px">getSysMonitor</td>
    <td style="padding:15px">{base_path}/{version}/sys/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMounts(callback)</td>
    <td style="padding:15px">List the currently mounted backends.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMountsPath(pathParam, body, callback)</td>
    <td style="padding:15px">Enable a new secrets engine at the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysMountsPath(pathParam, callback)</td>
    <td style="padding:15px">Disable the mount point specified at the given path.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysMountsPathTune(pathParam, callback)</td>
    <td style="padding:15px">Tune backend configuration parameters for this mount.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mounts/{pathv1}/tune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysMountsPathTune(pathParam, body, callback)</td>
    <td style="padding:15px">Tune backend configuration parameters for this mount.</td>
    <td style="padding:15px">{base_path}/{version}/sys/mounts/{pathv1}/tune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysNamespaces(list, callback)</td>
    <td style="padding:15px">getSysNamespaces</td>
    <td style="padding:15px">{base_path}/{version}/sys/namespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysNamespacesPath(pathParam, callback)</td>
    <td style="padding:15px">getSysNamespacesPath</td>
    <td style="padding:15px">{base_path}/{version}/sys/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysNamespacesPath(pathParam, callback)</td>
    <td style="padding:15px">postSysNamespacesPath</td>
    <td style="padding:15px">{base_path}/{version}/sys/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysNamespacesPath(pathParam, callback)</td>
    <td style="padding:15px">deleteSysNamespacesPath</td>
    <td style="padding:15px">{base_path}/{version}/sys/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPluginsCatalog(callback)</td>
    <td style="padding:15px">Lists all the plugins known to Vault</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPluginsCatalogName(name, callback)</td>
    <td style="padding:15px">Return the configuration data for the plugin with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPluginsCatalogName(name, body, callback)</td>
    <td style="padding:15px">Register a new plugin, or updates an existing one with the supplied name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPluginsCatalogName(name, callback)</td>
    <td style="padding:15px">Remove the plugin with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPluginsCatalogType(type, list, callback)</td>
    <td style="padding:15px">List the plugins in the catalog.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPluginsCatalogTypeName(name, type, callback)</td>
    <td style="padding:15px">Return the configuration data for the plugin with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPluginsCatalogTypeName(name, type, body, callback)</td>
    <td style="padding:15px">Register a new plugin, or updates an existing one with the supplied name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPluginsCatalogTypeName(name, type, callback)</td>
    <td style="padding:15px">Remove the plugin with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/catalog/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPluginsReloadBackend(body, callback)</td>
    <td style="padding:15px">Reload mounted plugin backends.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/reload/backend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPluginsReloadBackendStatus(callback)</td>
    <td style="padding:15px">Get the status of a cluster-scoped reload.</td>
    <td style="padding:15px">{base_path}/{version}/sys/plugins/reload/backend/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesAcl(list, callback)</td>
    <td style="padding:15px">List the configured access control policies.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/acl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesAclName(name, callback)</td>
    <td style="padding:15px">Retrieve information about the named ACL policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPoliciesAclName(name, body, callback)</td>
    <td style="padding:15px">Add a new or update an existing ACL policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPoliciesAclName(name, callback)</td>
    <td style="padding:15px">Delete the ACL policy with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/acl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesEgp(list, callback)</td>
    <td style="padding:15px">List the configured access control policies.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/egp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesEgpName(name, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/egp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPoliciesEgpName(name, body, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/egp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPoliciesEgpName(name, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/egp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesPasswordName(name, callback)</td>
    <td style="padding:15px">Retrieve an existing password policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPoliciesPasswordName(name, body, callback)</td>
    <td style="padding:15px">Add a new or update an existing password policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPoliciesPasswordName(name, callback)</td>
    <td style="padding:15px">Delete a password policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesPasswordNameGenerate(name, callback)</td>
    <td style="padding:15px">Generate a password from an existing password policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/password/{pathv1}/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesRgp(list, callback)</td>
    <td style="padding:15px">List the configured access control policies.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/rgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPoliciesRgpName(name, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/rgp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPoliciesRgpName(name, body, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/rgp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPoliciesRgpName(name, callback)</td>
    <td style="padding:15px">Read, Modify, or Delete an access control policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policies/rgp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPolicy(list, callback)</td>
    <td style="padding:15px">List the configured access control policies.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPolicyName(name, callback)</td>
    <td style="padding:15px">Retrieve the policy body for the named policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysPolicyName(name, body, callback)</td>
    <td style="padding:15px">Add a new or update an existing policy.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysPolicyName(name, callback)</td>
    <td style="padding:15px">Delete the policy with the given name.</td>
    <td style="padding:15px">{base_path}/{version}/sys/policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprof(callback)</td>
    <td style="padding:15px">Returns an HTML page listing the available profiles.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofCmdline(callback)</td>
    <td style="padding:15px">Returns the running program's command line.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/cmdline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofGoroutine(callback)</td>
    <td style="padding:15px">Returns stack traces of all current goroutines.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/goroutine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofHeap(callback)</td>
    <td style="padding:15px">Returns a sampling of memory allocations of live object.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/heap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofProfile(callback)</td>
    <td style="padding:15px">Returns a pprof-formatted cpu profile payload.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofSymbol(callback)</td>
    <td style="padding:15px">Returns the program counters listed in the request.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/symbol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofTrace(callback)</td>
    <td style="padding:15px">Returns the execution trace in binary form.</td>
    <td style="padding:15px">{base_path}/{version}/sys/pprof/trace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysQuotasConfig(callback)</td>
    <td style="padding:15px">getSysQuotasConfig</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysQuotasConfig(body, callback)</td>
    <td style="padding:15px">postSysQuotasConfig</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysQuotasLeaseCount(list, callback)</td>
    <td style="padding:15px">getSysQuotasLeaseCount</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/lease-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysQuotasLeaseCountName(name, callback)</td>
    <td style="padding:15px">getSysQuotasLeaseCountName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/lease-count/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysQuotasLeaseCountName(name, body, callback)</td>
    <td style="padding:15px">postSysQuotasLeaseCountName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/lease-count/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysQuotasLeaseCountName(name, callback)</td>
    <td style="padding:15px">deleteSysQuotasLeaseCountName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/lease-count/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysQuotasRateLimit(list, callback)</td>
    <td style="padding:15px">getSysQuotasRateLimit</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/rate-limit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysQuotasRateLimitName(name, callback)</td>
    <td style="padding:15px">getSysQuotasRateLimitName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/rate-limit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysQuotasRateLimitName(name, body, callback)</td>
    <td style="padding:15px">postSysQuotasRateLimitName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/rate-limit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysQuotasRateLimitName(name, callback)</td>
    <td style="padding:15px">deleteSysQuotasRateLimitName</td>
    <td style="padding:15px">{base_path}/{version}/sys/quotas/rate-limit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysRekeyBackup(callback)</td>
    <td style="padding:15px">Return the backup copy of PGP-encrypted unseal keys.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysRekeyBackup(callback)</td>
    <td style="padding:15px">Delete the backup copy of PGP-encrypted unseal keys.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysRekeyInit(callback)</td>
    <td style="padding:15px">Reads the configuration and progress of the current rekey attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRekeyInit(body, callback)</td>
    <td style="padding:15px">Initializes a new rekey attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysRekeyInit(callback)</td>
    <td style="padding:15px">Cancels any in-progress rekey.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysRekeyRecoveryKeyBackup(callback)</td>
    <td style="padding:15px">Allows fetching or deleting the backup of the rotated unseal keys.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/recovery-key-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysRekeyRecoveryKeyBackup(callback)</td>
    <td style="padding:15px">Allows fetching or deleting the backup of the rotated unseal keys.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/recovery-key-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRekeyUpdate(body, callback)</td>
    <td style="padding:15px">Enter a single master key share to progress the rekey of the Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysRekeyVerify(callback)</td>
    <td style="padding:15px">Read the configuration and progress of the current rekey verification attempt.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRekeyVerify(body, callback)</td>
    <td style="padding:15px">Enter a single new key share to progress the rekey verification operation.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysRekeyVerify(callback)</td>
    <td style="padding:15px">Cancel any in-progress rekey verification operation.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rekey/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRemount(body, callback)</td>
    <td style="padding:15px">Move the mount point of an already-mounted backend.</td>
    <td style="padding:15px">{base_path}/{version}/sys/remount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRenew(body, callback)</td>
    <td style="padding:15px">Renews a lease, requesting to extend the lease.</td>
    <td style="padding:15px">{base_path}/{version}/sys/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRenewUrlLeaseId(urlLeaseId, body, callback)</td>
    <td style="padding:15px">Renews a lease, requesting to extend the lease.</td>
    <td style="padding:15px">{base_path}/{version}/sys/renew/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrPrimaryDemote(callback)</td>
    <td style="padding:15px">postSysReplicationDrPrimaryDemote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/primary/demote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrPrimaryDisable(callback)</td>
    <td style="padding:15px">postSysReplicationDrPrimaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/primary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrPrimaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrPrimaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/primary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrPrimaryRevokeSecondary(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrPrimaryRevokeSecondary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/primary/revoke-secondary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrPrimarySecondaryToken(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrPrimarySecondaryToken</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/primary/secondary-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryDisable(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryGeneratePublicKey(callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryGeneratePublicKey</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/generate-public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationDrSecondaryLicense(callback)</td>
    <td style="padding:15px">The path responds to the following HTTP methods.

    GET /
        Returns information on the inst</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryLicense(body, callback)</td>
    <td style="padding:15px">The path responds to the following HTTP methods.

    GET /
        Returns information on the inst</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryOperationTokenDelete(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryOperationTokenDelete</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/operation-token/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryPromote(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryPromote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/promote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryRecover(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryRecover</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/recover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryReindex(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryReindex</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/reindex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationDrSecondaryUpdatePrimary(body, callback)</td>
    <td style="padding:15px">postSysReplicationDrSecondaryUpdatePrimary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/secondary/update-primary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationDrStatus(callback)</td>
    <td style="padding:15px">getSysReplicationDrStatus</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/dr/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryDemote(callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryDemote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/demote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryDisable(callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationPerformancePrimaryDynamicFilterId(id, callback)</td>
    <td style="padding:15px">getSysReplicationPerformancePrimaryDynamicFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/dynamic-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationPerformancePrimaryMountFilterId(id, callback)</td>
    <td style="padding:15px">getSysReplicationPerformancePrimaryMountFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/mount-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryMountFilterId(id, body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryMountFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/mount-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysReplicationPerformancePrimaryMountFilterId(id, callback)</td>
    <td style="padding:15px">deleteSysReplicationPerformancePrimaryMountFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/mount-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationPerformancePrimaryPathsFilterId(id, callback)</td>
    <td style="padding:15px">getSysReplicationPerformancePrimaryPathsFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/paths-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryPathsFilterId(id, body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryPathsFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/paths-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysReplicationPerformancePrimaryPathsFilterId(id, callback)</td>
    <td style="padding:15px">deleteSysReplicationPerformancePrimaryPathsFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/paths-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimaryRevokeSecondary(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimaryRevokeSecondary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/revoke-secondary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformancePrimarySecondaryToken(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformancePrimarySecondaryToken</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/primary/secondary-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryDisable(callback)</td>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationPerformanceSecondaryDynamicFilterId(id, callback)</td>
    <td style="padding:15px">getSysReplicationPerformanceSecondaryDynamicFilterId</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/dynamic-filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryGeneratePublicKey(callback)</td>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryGeneratePublicKey</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/generate-public-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryPromote(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryPromote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/promote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryUpdatePrimary(body, callback)</td>
    <td style="padding:15px">postSysReplicationPerformanceSecondaryUpdatePrimary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/secondary/update-primary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationPerformanceStatus(callback)</td>
    <td style="padding:15px">getSysReplicationPerformanceStatus</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/performance/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPrimaryDemote(callback)</td>
    <td style="padding:15px">postSysReplicationPrimaryDemote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/primary/demote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPrimaryDisable(callback)</td>
    <td style="padding:15px">postSysReplicationPrimaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/primary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPrimaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationPrimaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/primary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPrimaryRevokeSecondary(body, callback)</td>
    <td style="padding:15px">postSysReplicationPrimaryRevokeSecondary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/primary/revoke-secondary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationPrimarySecondaryToken(body, callback)</td>
    <td style="padding:15px">postSysReplicationPrimarySecondaryToken</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/primary/secondary-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationRecover(callback)</td>
    <td style="padding:15px">postSysReplicationRecover</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/recover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationReindex(body, callback)</td>
    <td style="padding:15px">postSysReplicationReindex</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/reindex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationSecondaryDisable(callback)</td>
    <td style="padding:15px">postSysReplicationSecondaryDisable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/secondary/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationSecondaryEnable(body, callback)</td>
    <td style="padding:15px">postSysReplicationSecondaryEnable</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/secondary/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationSecondaryPromote(body, callback)</td>
    <td style="padding:15px">postSysReplicationSecondaryPromote</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/secondary/promote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysReplicationSecondaryUpdatePrimary(body, callback)</td>
    <td style="padding:15px">postSysReplicationSecondaryUpdatePrimary</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/secondary/update-primary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysReplicationStatus(callback)</td>
    <td style="padding:15px">getSysReplicationStatus</td>
    <td style="padding:15px">{base_path}/{version}/sys/replication/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRevoke(body, callback)</td>
    <td style="padding:15px">Revokes a lease immediately.</td>
    <td style="padding:15px">{base_path}/{version}/sys/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRevokeForcePrefix(prefix, callback)</td>
    <td style="padding:15px">Revokes all secrets or tokens generated under a given prefix immediately</td>
    <td style="padding:15px">{base_path}/{version}/sys/revoke-force/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRevokePrefixPrefix(prefix, body, callback)</td>
    <td style="padding:15px">Revokes all secrets (via a lease ID prefix) or tokens (via the tokens' path property) generated und</td>
    <td style="padding:15px">{base_path}/{version}/sys/revoke-prefix/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRevokeUrlLeaseId(urlLeaseId, body, callback)</td>
    <td style="padding:15px">Revokes a lease immediately.</td>
    <td style="padding:15px">{base_path}/{version}/sys/revoke/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRotate(callback)</td>
    <td style="padding:15px">Rotates the backend encryption key used to persist data.</td>
    <td style="padding:15px">{base_path}/{version}/sys/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysSeal(callback)</td>
    <td style="padding:15px">Seal the Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/seal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysSealStatus(callback)</td>
    <td style="padding:15px">Check the seal status of a Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/seal-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysSealwrapRewrap(callback)</td>
    <td style="padding:15px">Retrieve the state of any ongoing seal rewrap process</td>
    <td style="padding:15px">{base_path}/{version}/sys/sealwrap/rewrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysSealwrapRewrap(callback)</td>
    <td style="padding:15px">Start a seal rewrap process</td>
    <td style="padding:15px">{base_path}/{version}/sys/sealwrap/rewrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStepDown(callback)</td>
    <td style="padding:15px">Cause the node to give up active status.</td>
    <td style="padding:15px">{base_path}/{version}/sys/step-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftBootstrapAnswer(body, callback)</td>
    <td style="padding:15px">Accepts an answer from the peer to be joined to the fact cluster.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/bootstrap/answer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftBootstrapChallenge(body, callback)</td>
    <td style="padding:15px">Creates a challenge for the new peer to be joined to the raft cluster.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/bootstrap/challenge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftConfiguration(callback)</td>
    <td style="padding:15px">Returns the configuration of the raft cluster.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftRemovePeer(body, callback)</td>
    <td style="padding:15px">Remove a peer from the raft cluster.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/remove-peer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftSnapshot(callback)</td>
    <td style="padding:15px">Returns a snapshot of the current state of vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftSnapshot(callback)</td>
    <td style="padding:15px">Installs the provided snapshot, returning the cluster to the state defined in it.</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftSnapshotForce(callback)</td>
    <td style="padding:15px">Installs the provided snapshot, returning the cluster to the state defined in it. This bypasses che</td>
    <td style="padding:15px">{base_path}/{version}/sys/storage/raft/snapshot-force?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysToolsHash(body, callback)</td>
    <td style="padding:15px">Generate a hash sum for input data</td>
    <td style="padding:15px">{base_path}/{version}/sys/tools/hash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysToolsHashUrlalgorithm(urlalgorithm, body, callback)</td>
    <td style="padding:15px">Generate a hash sum for input data</td>
    <td style="padding:15px">{base_path}/{version}/sys/tools/hash/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysToolsRandom(body, callback)</td>
    <td style="padding:15px">Generate random bytes</td>
    <td style="padding:15px">{base_path}/{version}/sys/tools/random?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysToolsRandomUrlbytes(urlbytes, body, callback)</td>
    <td style="padding:15px">Generate random bytes</td>
    <td style="padding:15px">{base_path}/{version}/sys/tools/random/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysUnseal(body, callback)</td>
    <td style="padding:15px">Unseal the Vault.</td>
    <td style="padding:15px">{base_path}/{version}/sys/unseal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysWrappingLookup(callback)</td>
    <td style="padding:15px">Look up wrapping properties for the requester's token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/wrapping/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysWrappingLookup(body, callback)</td>
    <td style="padding:15px">Look up wrapping properties for the given token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/wrapping/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysWrappingRewrap(body, callback)</td>
    <td style="padding:15px">Rotates a response-wrapped token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/wrapping/rewrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysWrappingUnwrap(body, callback)</td>
    <td style="padding:15px">Unwraps a response-wrapped token.</td>
    <td style="padding:15px">{base_path}/{version}/sys/wrapping/unwrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysWrappingWrap(callback)</td>
    <td style="padding:15px">Response-wraps an arbitrary JSON object.</td>
    <td style="padding:15px">{base_path}/{version}/sys/wrapping/wrap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdConfig(namespace, admount, callback)</td>
    <td style="padding:15px">getAdConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdConfig(namespace, admount, body, callback)</td>
    <td style="padding:15px">createAdConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdConfig(namespace, admount, callback)</td>
    <td style="padding:15px">deleteAdEngineConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdCredsName(namespace, admount, name, callback)</td>
    <td style="padding:15px">getAdCredsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdLibrary(namespace, admount, list, callback)</td>
    <td style="padding:15px">getAdLibrary</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdLibraryManageNameCheckIn(namespace, admount, name, body, callback)</td>
    <td style="padding:15px">createAdLibraryManageNameCheckIn</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/manage/{pathv3}/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdLibraryName(namespace, admount, name, callback)</td>
    <td style="padding:15px">getAdLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdLibraryName(namespace, admount, name, body, callback)</td>
    <td style="padding:15px">updateAdLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdLibraryName(namespace, admount, name, callback)</td>
    <td style="padding:15px">deleteAdLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdLibraryNameCheckIn(namespace, admount, name, body, callback)</td>
    <td style="padding:15px">createAdLibraryNameCheckIn</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdLibraryNameCheckOut(namespace, admount, name, body, callback)</td>
    <td style="padding:15px">createAdLibraryNameCheckOut</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/check-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdLibraryNameStatus(namespace, admount, name, callback)</td>
    <td style="padding:15px">getAdLibraryNameStatus</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdRotateRoot(namespace, admount, callback)</td>
    <td style="padding:15px">getAdRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdRotateRoot(namespace, admount, callback)</td>
    <td style="padding:15px">createAdRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlicloudRole(namespace, alicloudmount, list, callback)</td>
    <td style="padding:15px">getAlicloudRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlicloudRoleName(namespace, alicloudmount, name, callback)</td>
    <td style="padding:15px">getAlicloudRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlicloudRoleName(namespace, alicloudmount, name, body, callback)</td>
    <td style="padding:15px">postAlicloudRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlicloudRoleName(namespace, alicloudmount, name, callback)</td>
    <td style="padding:15px">deleteAlicloudRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAwsConfigLease(namespace, awsmount, callback)</td>
    <td style="padding:15px">getAwsConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAwsConfigLease(namespace, awsmount, body, callback)</td>
    <td style="padding:15px">postAwsConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAwsConfigRoot(namespace, awsmount, callback)</td>
    <td style="padding:15px">getAwsConfigRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAwsConfigRoot(namespace, awsmount, body, callback)</td>
    <td style="padding:15px">postAwsConfigRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAwsConfigRotateRoot(namespace, awsmount, callback)</td>
    <td style="padding:15px">postAwsConfigRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAwsCreds(namespace, awsmount, callback)</td>
    <td style="padding:15px">getAwsCreds</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAwsCreds(namespace, awsmount, body, callback)</td>
    <td style="padding:15px">postAwsCreds</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAwsStsName(namespace, awsmount, name, callback)</td>
    <td style="padding:15px">getAwsStsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAwsStsName(namespace, awsmount, name, body, callback)</td>
    <td style="padding:15px">postAwsStsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureCredsRole(namespace, azuremount, role, callback)</td>
    <td style="padding:15px">getAzureCredsRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsulConfigAccess(namespace, consulmount, callback)</td>
    <td style="padding:15px">getConsulConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConsulConfigAccess(namespace, consulmount, body, callback)</td>
    <td style="padding:15px">postConsulConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsulCredsRole(namespace, consulmount, role, callback)</td>
    <td style="padding:15px">getConsulCredsRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCubbyholePath(namespace, cubbyholemount, enginepath, list, callback)</td>
    <td style="padding:15px">getCubbyholePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCubbyholePath(namespace, cubbyholemount, enginepath, callback)</td>
    <td style="padding:15px">postCubbyholePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCubbyholePath(namespace, cubbyholemount, enginepath, callback)</td>
    <td style="padding:15px">deleteCubbyholePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatabaseConfigName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">getDatabaseConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatabaseConfigName(namespace, databasemount, name, body, callback)</td>
    <td style="padding:15px">postDatabaseConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDatabaseConfigName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">deleteDatabaseConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatabaseResetName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">postDatabaseResetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/reset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatabaseRotateRoleName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">postDatabaseRotateRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatabaseRotateRootName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">postDatabaseRotateRootName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatabaseStaticCredsName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">getDatabaseStaticCredsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatabaseStaticRoles(namespace, databasemount, list, callback)</td>
    <td style="padding:15px">getDatabaseStaticRoles</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatabaseStaticRolesName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">getDatabaseStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatabaseStaticRolesName(namespace, databasemount, name, body, callback)</td>
    <td style="padding:15px">postDatabaseStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDatabaseStaticRolesName(namespace, databasemount, name, callback)</td>
    <td style="padding:15px">deleteDatabaseStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpConfigRotateRoot(namespace, gcpmount, callback)</td>
    <td style="padding:15px">postGcpConfigRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpKeyRoleset(namespace, gcpmount, roleset, callback)</td>
    <td style="padding:15px">getGcpKeyRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/key/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpKeyRoleset(namespace, gcpmount, roleset, body, callback)</td>
    <td style="padding:15px">postGcpKeyRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/key/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpRolesetName(namespace, gcpmount, name, callback)</td>
    <td style="padding:15px">getGcpRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpRolesetName(namespace, gcpmount, name, body, callback)</td>
    <td style="padding:15px">postGcpRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGcpRolesetName(namespace, gcpmount, name, callback)</td>
    <td style="padding:15px">deleteGcpRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpRolesetNameRotate(namespace, gcpmount, name, callback)</td>
    <td style="padding:15px">postGcpRolesetNameRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpRolesetNameRotateKey(namespace, gcpmount, name, callback)</td>
    <td style="padding:15px">postGcpRolesetNameRotateKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}/rotate-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpRolesets(namespace, gcpmount, list, callback)</td>
    <td style="padding:15px">getGcpRolesets</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rolesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpTokenRoleset(namespace, gcpmount, roleset, callback)</td>
    <td style="padding:15px">getGcpTokenRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/token/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpTokenRoleset(namespace, gcpmount, roleset, callback)</td>
    <td style="padding:15px">postGcpTokenRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/token/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsDecryptKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsDecryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/decrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsEncryptKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsEncryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/encrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpkmsKeys(namespace, gcpkmsmount, list, callback)</td>
    <td style="padding:15px">getGcpkmsKeys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpkmsKeysConfigKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">getGcpkmsKeysConfigKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysConfigKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsKeysConfigKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysDeregisterKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">postGcpkmsKeysDeregisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/deregister/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGcpkmsKeysDeregisterKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">deleteGcpkmsKeysDeregisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/deregister/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysRegisterKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsKeysRegisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/register/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysRotateKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">postGcpkmsKeysRotateKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/rotate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysTrimKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">postGcpkmsKeysTrimKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/trim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGcpkmsKeysTrimKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">deleteGcpkmsKeysTrimKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/trim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpkmsKeysKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">getGcpkmsKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsKeysKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGcpkmsKeysKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">deleteGcpkmsKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGcpkmsPubkeyKey(namespace, gcpkmsmount, key, callback)</td>
    <td style="padding:15px">getGcpkmsPubkeyKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pubkey/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsReencryptKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsReencryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/reencrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsSignKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsSignKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGcpkmsVerifyKey(namespace, gcpkmsmount, key, body, callback)</td>
    <td style="padding:15px">postGcpkmsVerifyKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKvDataPath(namespace, kvmount, enginepath, callback)</td>
    <td style="padding:15px">getKvDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvDataPath(namespace, kvmount, enginepath, body, callback)</td>
    <td style="padding:15px">postKvDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKvDataPath(namespace, kvmount, enginepath, callback)</td>
    <td style="padding:15px">deleteKvDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvDeletePath(namespace, kvmount, enginepath, body, callback)</td>
    <td style="padding:15px">postKvDeletePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/delete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvDestroyPath(namespace, kvmount, enginepath, body, callback)</td>
    <td style="padding:15px">postKvDestroyPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/destroy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKvMetadataPath(namespace, kvmount, enginepath, list, callback)</td>
    <td style="padding:15px">getKvMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvMetadataPath(namespace, kvmount, enginepath, body, callback)</td>
    <td style="padding:15px">postKvMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKvMetadataPath(namespace, kvmount, enginepath, callback)</td>
    <td style="padding:15px">deleteKvMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postKvUndeletePath(namespace, kvmount, enginepath, body, callback)</td>
    <td style="padding:15px">postKvUndeletePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/undelete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNomadConfigAccess(namespace, nomadmount, callback)</td>
    <td style="padding:15px">getNomadConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNomadConfigAccess(namespace, nomadmount, body, callback)</td>
    <td style="padding:15px">postNomadConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNomadConfigAccess(namespace, nomadmount, callback)</td>
    <td style="padding:15px">deleteNomadConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNomadConfigLease(namespace, nomadmount, callback)</td>
    <td style="padding:15px">getNomadConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNomadConfigLease(namespace, nomadmount, body, callback)</td>
    <td style="padding:15px">postNomadConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNomadConfigLease(namespace, nomadmount, callback)</td>
    <td style="padding:15px">deleteNomadConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNomadRole(namespace, nomadmount, list, callback)</td>
    <td style="padding:15px">getNomadRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNomadRoleName(namespace, nomadmount, name, callback)</td>
    <td style="padding:15px">getNomadRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNomadRoleName(namespace, nomadmount, name, body, callback)</td>
    <td style="padding:15px">postNomadRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNomadRoleName(namespace, nomadmount, name, callback)</td>
    <td style="padding:15px">deleteNomadRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCa(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCaPem(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCaPem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCaChain(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCa_chain</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCertCaChain(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCertCa_chain</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCertCrl(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCertCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCertSerial(namespace, pkimount, serial, callback)</td>
    <td style="padding:15px">getPkiCertSerial</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCerts(namespace, pkimount, list, callback)</td>
    <td style="padding:15px">getPkiCerts</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/certs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiConfigCa(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiConfigCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiConfigCrl(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiConfigCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiConfigCrl(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiConfigCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiConfigUrls(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiConfigUrls</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiConfigUrls(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiConfigUrls</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCrl(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCrlPem(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCrlPem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPkiCrlRotate(namespace, pkimount, callback)</td>
    <td style="padding:15px">getPkiCrlRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiIntermediateGenerateExported(namespace, pkimount, exported, body, callback)</td>
    <td style="padding:15px">postPkiIntermediateGenerateExported</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiIntermediateSetSigned(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiIntermediateSetSigned</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/set-signed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiIssueRole(namespace, pkimount, role, body, callback)</td>
    <td style="padding:15px">postPkiIssueRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/issue/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiRevoke(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiRevoke</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePkiRoot(namespace, pkimount, callback)</td>
    <td style="padding:15px">deletePkiRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiRootGenerateExported(namespace, pkimount, exported, body, callback)</td>
    <td style="padding:15px">postPkiRootGenerateExported</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiRootSignIntermediate(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiRootSignIntermediate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-intermediate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiRootSignSelfIssued(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiRootSignSelfIssued</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-self-issued?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSignVerbatim(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiSignVerbatim</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSignVerbatimRole(namespace, pkimount, role, body, callback)</td>
    <td style="padding:15px">postPkiSignVerbatimRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiSignRole(namespace, pkimount, role, body, callback)</td>
    <td style="padding:15px">postPkiSignRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPkiTidy(namespace, pkimount, body, callback)</td>
    <td style="padding:15px">postPkiTidy</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tidy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRabbitmqConfigConnection(namespace, rabbitmqmount, body, callback)</td>
    <td style="padding:15px">postRabbitmqConfigConnection</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRabbitmqConfigLease(namespace, rabbitmqmount, callback)</td>
    <td style="padding:15px">getRabbitmqConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRabbitmqConfigLease(namespace, rabbitmqmount, body, callback)</td>
    <td style="padding:15px">postRabbitmqConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfig(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfig(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineConfig(namespace, secretmount, callback)</td>
    <td style="padding:15px">deleteSecretEngineConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineCredsName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineCredsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineLibrary(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineLibrary</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineLibraryManageNameCheckIn(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineLibraryManageNameCheckIn</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/manage/{pathv3}/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineLibraryName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecretEngineLibraryName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">updateSecretEngineLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineLibraryName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineLibraryName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineLibraryNameCheckIn(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineLibraryNameCheckIn</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/check-in?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineLibraryNameCheckOut(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineLibraryNameCheckOut</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/check-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineLibraryNameStatus(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineLibraryNameStatus</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/library/{pathv3}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRotateRoot(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRotateRoot(namespace, secretmount, callback)</td>
    <td style="padding:15px">postSecretEngineRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRole(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRoleName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRoleName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineRoleName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfigLease(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigLease(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigLease</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfigRoot(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineConfigRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigRoot(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigRotateRoot(namespace, secretmount, callback)</td>
    <td style="padding:15px">postSecretEngineConfigRotateRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/rotate-root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineCreds(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineCreds</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineCreds(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineCreds</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineStsName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineStsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineStsName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineStsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineCredsRole(namespace, secretmount, role, callback)</td>
    <td style="padding:15px">getSecretEngineCredsRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfigAccess(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigAccess(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigAccess</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEnginePath(namespace, secretmount, secretpath, list, callback)</td>
    <td style="padding:15px">getSecretEnginePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEnginePath(namespace, secretmount, secretpath, callback)</td>
    <td style="padding:15px">postSecretEnginePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEnginePath(namespace, secretmount, secretpath, callback)</td>
    <td style="padding:15px">deleteSecretEnginePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfigName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineConfigName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineResetName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineResetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/reset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRotateRoleName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineRotateRoleName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRotateRootName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineRotateRootName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rotate-root/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineStaticCredsName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineStaticCredsName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineStaticRoles(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineStaticRoles</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineStaticRolesName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineStaticRolesName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineStaticRolesName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineStaticRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/static-roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineKeyRoleset(namespace, secretmount, roleset, callback)</td>
    <td style="padding:15px">getSecretEngineKeyRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/key/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeyRoleset(namespace, secretmount, roleset, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeyRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/key/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRolesetName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRolesetName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineRolesetName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineRolesetName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRolesetNameRotate(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineRolesetNameRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRolesetNameRotateKey(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineRolesetNameRotateKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roleset/{pathv3}/rotate-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRolesets(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineRolesets</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rolesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineTokenRoleset(namespace, secretmount, roleset, callback)</td>
    <td style="padding:15px">getSecretEngineTokenRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/token/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineTokenRoleset(namespace, secretmount, roleset, callback)</td>
    <td style="padding:15px">postSecretEngineTokenRoleset</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/token/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDecryptKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineDecryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/decrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineEncryptKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineEncryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/encrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineKeys(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineKeys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineKeysConfigKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">getSecretEngineKeysConfigKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysConfigKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysConfigKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/config/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysDeregisterKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">postSecretEngineKeysDeregisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/deregister/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineKeysDeregisterKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">deleteSecretEngineKeysDeregisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/deregister/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysRegisterKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysRegisterKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/register/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysRotateKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">postSecretEngineKeysRotateKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/rotate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysTrimKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">postSecretEngineKeysTrimKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/trim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineKeysTrimKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">deleteSecretEngineKeysTrimKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/trim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineKeysKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">getSecretEngineKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineKeysKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">deleteSecretEngineKeysKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEnginePubkeyKey(namespace, secretmount, key, callback)</td>
    <td style="padding:15px">getSecretEnginePubkeyKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pubkey/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineReencryptKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineReencryptKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/reencrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineSignKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineSignKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineVerifyKey(namespace, secretmount, key, body, callback)</td>
    <td style="padding:15px">postSecretEngineVerifyKey</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineDataPath(namespace, secretmount, enginepath, callback)</td>
    <td style="padding:15px">getSecretEngineDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDataPath(namespace, secretmount, enginepath, body, callback)</td>
    <td style="padding:15px">postSecretEngineDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineDataPath(namespace, secretmount, enginepath, callback)</td>
    <td style="padding:15px">deleteSecretEngineDataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/data/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDeletePath(namespace, secretmount, enginepath, body, callback)</td>
    <td style="padding:15px">postSecretEngineDeletePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/delete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDestroyPath(namespace, secretmount, enginepath, body, callback)</td>
    <td style="padding:15px">postSecretEngineDestroyPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/destroy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineMetadataPath(namespace, secretmount, enginepath, list, callback)</td>
    <td style="padding:15px">getSecretEngineMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineMetadataPath(namespace, secretmount, enginepath, body, callback)</td>
    <td style="padding:15px">postSecretEngineMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineMetadataPath(namespace, secretmount, enginepath, callback)</td>
    <td style="padding:15px">deleteSecretEngineMetadataPath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineUndeletePath(namespace, secretmount, enginepath, body, callback)</td>
    <td style="padding:15px">postSecretEngineUndeletePath</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/undelete/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigConnection(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigConnection</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineConfigZeroaddress(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineConfigZeroaddress(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineConfigZeroaddress(namespace, secretmount, callback)</td>
    <td style="padding:15px">deleteSecretEngineConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysKeyName(namespace, secretmount, keyName, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysKey_name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineKeysKeyName(namespace, secretmount, keyName, callback)</td>
    <td style="padding:15px">deleteSecretEngineKeysKey_name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineLookup(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineLookup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEnginePublicKey(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEnginePublic_key</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/public_key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineVerify(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineVerify</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCa(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCaPem(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCaPem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCaChain(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCa_chain</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCertCaChain(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCertCa_chain</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/ca_chain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCertCrl(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCertCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertSerial(namespace, secretmount, serial, callback)</td>
    <td style="padding:15px">getCertSerial</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cert/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCerts(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getCertCerts</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/certs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertConfigCa(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertConfigCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertConfigCrl(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertConfigCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertConfigCrl(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertConfigCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertConfigUrls(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertConfigUrls</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertConfigUrls(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertConfigUrls</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCrl(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCrl</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCrlPem(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCrlPem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/pem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertCrlRotate(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertCrlRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/crl/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertImportQueue(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getCertImportQueue</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/import-queue/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertImportQueueRole(namespace, secretmount, role, callback)</td>
    <td style="padding:15px">getCertImportQueueRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/import-queue/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertIntermediateGenerateExported(namespace, secretmount, exported, body, callback)</td>
    <td style="padding:15px">postCertIntermediateGenerateExported</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertIntermediateSetSigned(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertIntermediateSetSigned</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/intermediate/set-signed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertIssueRole(namespace, secretmount, role, body, callback)</td>
    <td style="padding:15px">postCertIssueRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/issue/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertRevoke(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertRevoke</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRoles(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getSecretEngineRoles</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineRolesName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRolesName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineRolesName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineRolesName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/roles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertRoot(namespace, secretmount, callback)</td>
    <td style="padding:15px">deleteCertRoot</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertRootGenerateExported(namespace, secretmount, exported, body, callback)</td>
    <td style="padding:15px">postCertRootGenerateExported</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/generate/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertRootSignIntermediate(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertRootSignIntermediate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-intermediate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertRootSignSelfIssued(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertRootSignSelfIssued</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/root/sign-self-issued?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertShowVenafiRolePolicyMap(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertShowVenafiRolePolicyMap</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/show-venafi-role-policy-map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertSignVerbatim(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertSignVerbatim</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertSignVerbatimRole(namespace, secretmount, role, body, callback)</td>
    <td style="padding:15px">postCertSignVerbatimRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign-verbatim/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertSignRole(namespace, secretmount, role, body, callback)</td>
    <td style="padding:15px">postCertSignRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertTidy(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postCertTidy</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tidy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafi(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getCertVenafi</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafiPolicy(namespace, secretmount, list, callback)</td>
    <td style="padding:15px">getCertVenafiPolicy</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafiPolicyName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getCertVenafiPolicyName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertVenafiPolicyName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postCertVenafiPolicyName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertVenafiPolicyName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteCertVenafiPolicyName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafiPolicyNamePolicy(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getCertVenafiPolicyNamePolicy</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/{pathv3}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertVenafiPolicyNamePolicy(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postCertVenafiPolicyNamePolicy</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-policy/{pathv3}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafiSyncPolicies(namespace, secretmount, callback)</td>
    <td style="padding:15px">getCertVenafiSyncPolicies</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi-sync-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertVenafiName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getCertVenafiName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCertVenafiName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postCertVenafiName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertVenafiName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteCertVenafiName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/venafi/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineCodeName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineCodeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/code/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineCodeName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineCodeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/code/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineKeysName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretEngineKeysName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">deleteSecretEngineKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineBackupName(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">getSecretEngineBackupName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/backup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineCacheConfig(namespace, secretmount, callback)</td>
    <td style="padding:15px">getSecretEngineCacheConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cache-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineCacheConfig(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineCacheConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cache-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDatakeyPlaintextName(namespace, secretmount, name, plaintext, body, callback)</td>
    <td style="padding:15px">postSecretEngineDatakeyPlaintextName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/datakey/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineDecryptName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineDecryptName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/decrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineEncryptName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineEncryptName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/encrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineExportTypeName(namespace, secretmount, name, type, callback)</td>
    <td style="padding:15px">getSecretEngineExportTypeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/export/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretEngineExportTypeNameVersion(namespace, secretmount, name, type, version, callback)</td>
    <td style="padding:15px">getSecretEngineExportTypeNameVersion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/export/{pathv3}/{pathv4}/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineHash(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineHash</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineHashUrlalgorithm(namespace, secretmount, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postSecretEngineHashUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hash/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineHmacName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineHmacName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hmac/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineHmacNameUrlalgorithm(namespace, secretmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postSecretEngineHmacNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hmac/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysNameConfig(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysNameConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysNameRotate(namespace, secretmount, name, callback)</td>
    <td style="padding:15px">postSecretEngineKeysNameRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineKeysNameTrim(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineKeysNameTrim</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/trim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRandom(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineRandom</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/random?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRandomUrlbytes(namespace, secretmount, urlbytes, body, callback)</td>
    <td style="padding:15px">postSecretEngineRandomUrlbytes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/random/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRestore(namespace, secretmount, body, callback)</td>
    <td style="padding:15px">postSecretEngineRestore</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRestoreName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineRestoreName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/restore/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineRewrapName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineRewrapName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rewrap/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineSignName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineSignName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineSignNameUrlalgorithm(namespace, secretmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postSecretEngineSignNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineVerifyName(namespace, secretmount, name, body, callback)</td>
    <td style="padding:15px">postSecretEngineVerifyName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretEngineVerifyNameUrlalgorithm(namespace, secretmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postSecretEngineVerifyNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshConfigCa(namespace, sshmount, callback)</td>
    <td style="padding:15px">getSshConfigCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshConfigCa(namespace, sshmount, body, callback)</td>
    <td style="padding:15px">postSshConfigCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSshConfigCa(namespace, sshmount, callback)</td>
    <td style="padding:15px">deleteSshConfigCa</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshConfigZeroaddress(namespace, sshmount, callback)</td>
    <td style="padding:15px">getSshConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshConfigZeroaddress(namespace, sshmount, body, callback)</td>
    <td style="padding:15px">postSshConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSshConfigZeroaddress(namespace, sshmount, callback)</td>
    <td style="padding:15px">deleteSshConfigZeroaddress</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/config/zeroaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshCredsRole(namespace, sshmount, role, body, callback)</td>
    <td style="padding:15px">postSshCredsRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/creds/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshKeysKeyName(namespace, sshmount, keyName, body, callback)</td>
    <td style="padding:15px">postSshKeysKey_name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSshKeysKeyName(namespace, sshmount, keyName, callback)</td>
    <td style="padding:15px">deleteSshKeysKey_name</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshLookup(namespace, sshmount, body, callback)</td>
    <td style="padding:15px">postSshLookup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSshPublicKey(namespace, sshmount, callback)</td>
    <td style="padding:15px">getSshPublic_key</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/public_key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshSignRole(namespace, sshmount, role, body, callback)</td>
    <td style="padding:15px">postSshSignRole</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSshVerify(namespace, sshmount, body, callback)</td>
    <td style="padding:15px">postSshVerify</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotpCodeName(namespace, totpmount, name, callback)</td>
    <td style="padding:15px">getTotpCodeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/code/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTotpCodeName(namespace, totpmount, name, body, callback)</td>
    <td style="padding:15px">postTotpCodeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/code/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotpKeys(namespace, totpmount, list, callback)</td>
    <td style="padding:15px">getTotpKeys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotpKeysName(namespace, totpmount, name, callback)</td>
    <td style="padding:15px">getTotpKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTotpKeysName(namespace, totpmount, name, body, callback)</td>
    <td style="padding:15px">postTotpKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTotpKeysName(namespace, totpmount, name, callback)</td>
    <td style="padding:15px">deleteTotpKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitBackupName(namespace, transitmount, name, callback)</td>
    <td style="padding:15px">getTransitBackupName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/backup/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitCacheConfig(namespace, transitmount, callback)</td>
    <td style="padding:15px">getTransitCacheConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cache-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitCacheConfig(namespace, transitmount, body, callback)</td>
    <td style="padding:15px">postTransitCacheConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/cache-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitDatakeyPlaintextName(namespace, transitmount, name, plaintext, body, callback)</td>
    <td style="padding:15px">postTransitDatakeyPlaintextName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/datakey/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitDecryptName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitDecryptName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/decrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitEncryptName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitEncryptName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/encrypt/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitExportTypeName(namespace, transitmount, name, type, callback)</td>
    <td style="padding:15px">getTransitExportTypeName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/export/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitExportTypeNameVersion(namespace, transitmount, name, type, version, callback)</td>
    <td style="padding:15px">getTransitExportTypeNameVersion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/export/{pathv3}/{pathv4}/{pathv5}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitHash(namespace, transitmount, body, callback)</td>
    <td style="padding:15px">postTransitHash</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitHashUrlalgorithm(namespace, transitmount, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postTransitHashUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hash/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitHmacName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitHmacName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hmac/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitHmacNameUrlalgorithm(namespace, transitmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postTransitHmacNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/hmac/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitKeys(namespace, transitmount, list, callback)</td>
    <td style="padding:15px">getTransitKeys</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitKeysName(namespace, transitmount, name, callback)</td>
    <td style="padding:15px">getTransitKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitKeysName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitKeysName(namespace, transitmount, name, callback)</td>
    <td style="padding:15px">deleteTransitKeysName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitKeysNameConfig(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitKeysNameConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitKeysNameRotate(namespace, transitmount, name, callback)</td>
    <td style="padding:15px">postTransitKeysNameRotate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitKeysNameTrim(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitKeysNameTrim</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/keys/{pathv3}/trim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitRandom(namespace, transitmount, body, callback)</td>
    <td style="padding:15px">postTransitRandom</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/random?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitRandomUrlbytes(namespace, transitmount, urlbytes, body, callback)</td>
    <td style="padding:15px">postTransitRandomUrlbytes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/random/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitRestore(namespace, transitmount, body, callback)</td>
    <td style="padding:15px">postTransitRestore</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitRestoreName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitRestoreName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/restore/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitRewrapName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitRewrapName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/rewrap/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitSignName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitSignName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitSignNameUrlalgorithm(namespace, transitmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postTransitSignNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/sign/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitVerifyName(namespace, transitmount, name, body, callback)</td>
    <td style="padding:15px">postTransitVerifyName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransitVerifyNameUrlalgorithm(namespace, transitmount, name, urlalgorithm, body, callback)</td>
    <td style="padding:15px">postTransitVerifyNameUrlalgorithm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/verify/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalCountersActivity(namespace, callback)</td>
    <td style="padding:15px">getSysInternalCountersActivity</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/internal/counters/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalCountersActivityMonthly(namespace, callback)</td>
    <td style="padding:15px">getSysInternalCountersActivityMonthly</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/internal/counters/activity/monthly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalCountersConfig(namespace, callback)</td>
    <td style="padding:15px">getSysInternalCountersConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/internal/counters/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysInternalCountersConfig(namespace, body, callback)</td>
    <td style="padding:15px">postSysInternalCountersConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/internal/counters/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysInternalUiFeatureFlags(namespace, callback)</td>
    <td style="padding:15px">getSysInternalUiFeatureFlags</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/internal/ui/feature-flags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofAllocs(namespace, callback)</td>
    <td style="padding:15px">getSysPprofAllocs</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/pprof/allocs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofBlock(namespace, callback)</td>
    <td style="padding:15px">getSysPprofBlock</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/pprof/block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofMutex(namespace, callback)</td>
    <td style="padding:15px">getSysPprofMutex</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/pprof/mutex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysPprofThreadcreate(namespace, callback)</td>
    <td style="padding:15px">getSysPprofThreadcreate</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/pprof/threadcreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysRotateConfig(namespace, callback)</td>
    <td style="padding:15px">getSysRotateConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/rotate/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysRotateConfig(namespace, body, callback)</td>
    <td style="padding:15px">postSysRotateConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/rotate/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftAutopilotConfiguration(namespace, callback)</td>
    <td style="padding:15px">getSysStorageRaftAutopilotConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/autopilot/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftAutopilotConfiguration(namespace, body, callback)</td>
    <td style="padding:15px">postSysStorageRaftAutopilotConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/autopilot/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftAutopilotState(namespace, callback)</td>
    <td style="padding:15px">getSysStorageRaftAutopilotState</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/autopilot/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoConfig(namespace, list, callback)</td>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoConfig</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/snapshot-auto/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoConfigName(namespace, name, callback)</td>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/snapshot-auto/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSysStorageRaftSnapshotAutoConfigName(namespace, name, body, callback)</td>
    <td style="padding:15px">postSysStorageRaftSnapshotAutoConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/snapshot-auto/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSysStorageRaftSnapshotAutoConfigName(namespace, name, callback)</td>
    <td style="padding:15px">deleteSysStorageRaftSnapshotAutoConfigName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/snapshot-auto/config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoStatusName(namespace, name, callback)</td>
    <td style="padding:15px">getSysStorageRaftSnapshotAutoStatusName</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/sys/storage/raft/snapshot-auto/status/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
