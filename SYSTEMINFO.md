# HashiCorp Vault

Vendor: HashiCorp
Homepage: https://www.hashicorp.com/

Product: Vault
Product Page: https://www.hashicorp.com/products/vault

## Introduction
We classify HashiCorp Vault into the ITSM or Service Management domain as HashiCorp Vault provides a solution for managing secrets and sensitive data within ITSM processes.

## Why Integrate
The HashiCorp Vault adapter from Itential is used to integrate the Itential Automation Platform (IAP) with HashiCorp Vault. With this adapter you have the ability to perform operations on items such as:

- Secrets
- Identity

## Additional Product Documentation
[Vault API Documentation](https://developer.hashicorp.com/vault/api-docs)