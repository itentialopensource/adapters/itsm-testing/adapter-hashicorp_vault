/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-hashicorp_vault',
      type: 'HashiCorpVault',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const HashiCorpVault = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] HashiCorpVault Adapter Test', () => {
  describe('HashiCorpVault Class Tests', () => {
    const a = new HashiCorpVault(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('hashicorp_vault'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('hashicorp_vault'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('HashiCorpVault', pronghornDotJson.export);
          assert.equal('HashiCorpVault', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-hashicorp_vault', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('hashicorp_vault'));
          assert.equal('HashiCorpVault', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-hashicorp_vault', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-hashicorp_vault', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getKvSecretConfig - errors', () => {
      it('should have a getKvSecretConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getKvSecretConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretConfig - errors', () => {
      it('should have a postKvSecretConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKvSecretDataPath - errors', () => {
      it('should have a getKvSecretDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getKvSecretDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getKvSecretDataPath(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getKvSecretDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretDataPath - errors', () => {
      it('should have a postKvSecretDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postKvSecretDataPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvSecretDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKvSecretDataPath - errors', () => {
      it('should have a deleteKvSecretDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKvSecretDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteKvSecretDataPath(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteKvSecretDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretDeletePath - errors', () => {
      it('should have a postKvSecretDeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretDeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postKvSecretDeletePath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvSecretDeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretDestroyPath - errors', () => {
      it('should have a postKvSecretDestroyPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretDestroyPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postKvSecretDestroyPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvSecretDestroyPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKvSecretMetadataPath - errors', () => {
      it('should have a getKvSecretMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getKvSecretMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getKvSecretMetadataPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getKvSecretMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretMetadataPath - errors', () => {
      it('should have a postKvSecretMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postKvSecretMetadataPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvSecretMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKvSecretMetadataPath - errors', () => {
      it('should have a deleteKvSecretMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKvSecretMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteKvSecretMetadataPath(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteKvSecretMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvSecretUndeletePath - errors', () => {
      it('should have a postKvSecretUndeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvSecretUndeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postKvSecretUndeletePath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvSecretUndeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCubbyholeSecretPath - errors', () => {
      it('should have a getCubbyholeSecretPath function', (done) => {
        try {
          assert.equal(true, typeof a.getCubbyholeSecretPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getCubbyholeSecretPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCubbyholeSecretPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCubbyholeSecretPath - errors', () => {
      it('should have a postCubbyholeSecretPath function', (done) => {
        try {
          assert.equal(true, typeof a.postCubbyholeSecretPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postCubbyholeSecretPath(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCubbyholeSecretPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCubbyholeSecretPath - errors', () => {
      it('should have a deleteCubbyholeSecretPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCubbyholeSecretPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteCubbyholeSecretPath(null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteCubbyholeSecretPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCa - errors', () => {
      it('should have a getPkiSecretCa function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCaPem - errors', () => {
      it('should have a getPkiSecretCaPem function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCaPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCaChain - errors', () => {
      it('should have a getPkiSecretCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCertCaChain - errors', () => {
      it('should have a getPkiSecretCertCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCertCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCertCrl - errors', () => {
      it('should have a getPkiSecretCertCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCertCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCertSerial - errors', () => {
      it('should have a getPkiSecretCertSerial function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCertSerial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getPkiSecretCertSerial(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getPkiSecretCertSerial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCerts - errors', () => {
      it('should have a getPkiSecretCerts function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretConfigCa - errors', () => {
      it('should have a postPkiSecretConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretConfigCrl - errors', () => {
      it('should have a getPkiSecretConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretConfigCrl - errors', () => {
      it('should have a postPkiSecretConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretConfigUrls - errors', () => {
      it('should have a getPkiSecretConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretConfigUrls - errors', () => {
      it('should have a postPkiSecretConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCrl - errors', () => {
      it('should have a getPkiSecretCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCrlPem - errors', () => {
      it('should have a getPkiSecretCrlPem function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCrlPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretCrlRotate - errors', () => {
      it('should have a getPkiSecretCrlRotate function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretCrlRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretGenerateIntermediate - errors', () => {
      it('should have a postPkiSecretGenerateIntermediate function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretGenerateIntermediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postPkiSecretGenerateIntermediate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretGenerateIntermediate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretSetSignedIntermediate - errors', () => {
      it('should have a postPkiSecretSetSignedIntermediate function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretSetSignedIntermediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretIssueCert - errors', () => {
      it('should have a postPkiSecretIssueCert function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretIssueCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiSecretIssueCert(null, null, null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretIssueCert', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretRevokeCert - errors', () => {
      it('should have a postPkiSecretRevokeCert function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretRevokeCert === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPkiSecretRoles - errors', () => {
      it('should have a listPkiSecretRoles function', (done) => {
        try {
          assert.equal(true, typeof a.listPkiSecretRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiSecretRolesName - errors', () => {
      it('should have a getPkiSecretRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiSecretRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getPkiSecretRolesName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getPkiSecretRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretRolesName - errors', () => {
      it('should have a postPkiSecretRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postPkiSecretRolesName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePkiSecretRolesName - errors', () => {
      it('should have a deletePkiSecretRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.deletePkiSecretRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deletePkiSecretRolesName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deletePkiSecretRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePkiSecretRoot - errors', () => {
      it('should have a deletePkiSecretRoot function', (done) => {
        try {
          assert.equal(true, typeof a.deletePkiSecretRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretGenerateRoot - errors', () => {
      it('should have a postPkiSecretGenerateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretGenerateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postPkiSecretGenerateRoot(null, null, null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretGenerateRoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretSignIntermediateRoot - errors', () => {
      it('should have a postPkiSecretSignIntermediateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretSignIntermediateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretRootSignSelfIssued - errors', () => {
      it('should have a postPkiSecretRootSignSelfIssued function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretRootSignSelfIssued === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretSignVerbatim - errors', () => {
      it('should have a postPkiSecretSignVerbatim function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretSignVerbatim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretSignVerbatimName - errors', () => {
      it('should have a postPkiSecretSignVerbatimName function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretSignVerbatimName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiSecretSignVerbatimName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretSignVerbatimName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretSignRole - errors', () => {
      it('should have a postPkiSecretSignRole function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretSignRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiSecretSignRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSecretSignRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSecretTidy - errors', () => {
      it('should have a postPkiSecretTidy function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSecretTidy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleLogin - errors', () => {
      it('should have a postAuthApproleLogin function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRole - errors', () => {
      it('should have a getAuthApproleRole function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleName - errors', () => {
      it('should have a getAuthApproleRoleRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleName - errors', () => {
      it('should have a postAuthApproleRoleRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleName(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleName - errors', () => {
      it('should have a deleteAuthApproleRoleRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameBindSecretId - errors', () => {
      it('should have a getAuthApproleRoleRoleNameBindSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameBindSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameBindSecretId(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameBindSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameBindSecretId - errors', () => {
      it('should have a postAuthApproleRoleRoleNameBindSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameBindSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameBindSecretId(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameBindSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameBindSecretId - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameBindSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameBindSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameBindSecretId(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameBindSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameBoundCidrList - errors', () => {
      it('should have a getAuthApproleRoleRoleNameBoundCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameBoundCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameBoundCidrList(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameBoundCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameBoundCidrList - errors', () => {
      it('should have a postAuthApproleRoleRoleNameBoundCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameBoundCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameBoundCidrList(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameBoundCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameBoundCidrList - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameBoundCidrList function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameBoundCidrList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameBoundCidrList(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameBoundCidrList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameCustomSecretId - errors', () => {
      it('should have a postAuthApproleRoleRoleNameCustomSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameCustomSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameCustomSecretId(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameCustomSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameLocalSecretIds - errors', () => {
      it('should have a getAuthApproleRoleRoleNameLocalSecretIds function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameLocalSecretIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameLocalSecretIds(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameLocalSecretIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNamePeriod - errors', () => {
      it('should have a getAuthApproleRoleRoleNamePeriod function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNamePeriod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNamePeriod(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNamePeriod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNamePeriod - errors', () => {
      it('should have a postAuthApproleRoleRoleNamePeriod function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNamePeriod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNamePeriod(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNamePeriod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNamePeriod - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNamePeriod function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNamePeriod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNamePeriod(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNamePeriod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNamePolicies - errors', () => {
      it('should have a getAuthApproleRoleRoleNamePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNamePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNamePolicies(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNamePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNamePolicies - errors', () => {
      it('should have a postAuthApproleRoleRoleNamePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNamePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNamePolicies(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNamePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNamePolicies - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNamePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNamePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNamePolicies(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNamePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameRoleId - errors', () => {
      it('should have a getAuthApproleRoleRoleNameRoleId function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameRoleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameRoleId(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameRoleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameRoleId - errors', () => {
      it('should have a postAuthApproleRoleRoleNameRoleId function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameRoleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameRoleId(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameRoleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameSecretId - errors', () => {
      it('should have a getAuthApproleRoleRoleNameSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameSecretId(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretId - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretId function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretId(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdAccessorDestroy - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdAccessorDestroy function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdAccessorDestroy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdAccessorDestroy(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdAccessorDestroy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameSecretIdAccessorDestroy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdAccessorLookup - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdAccessorLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdAccessorLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdAccessorLookup(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdAccessorLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameSecretIdBoundCidrs - errors', () => {
      it('should have a getAuthApproleRoleRoleNameSecretIdBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameSecretIdBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameSecretIdBoundCidrs(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameSecretIdBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdBoundCidrs - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdBoundCidrs(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameSecretIdBoundCidrs - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameSecretIdBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameSecretIdBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameSecretIdBoundCidrs(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameSecretIdBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameSecretIdNumUses - errors', () => {
      it('should have a getAuthApproleRoleRoleNameSecretIdNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameSecretIdNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameSecretIdNumUses(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameSecretIdNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdNumUses - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdNumUses(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameSecretIdNumUses - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameSecretIdNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameSecretIdNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameSecretIdNumUses(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameSecretIdNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameSecretIdTtl - errors', () => {
      it('should have a getAuthApproleRoleRoleNameSecretIdTtl function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameSecretIdTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameSecretIdTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameSecretIdTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdTtl - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdTtl function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdTtl(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameSecretIdTtl - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameSecretIdTtl function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameSecretIdTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameSecretIdTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameSecretIdTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdDestroy - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdDestroy function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdDestroy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdDestroy(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdDestroy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameSecretIdDestroy - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameSecretIdDestroy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameSecretIdDestroy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameSecretIdDestroy(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameSecretIdDestroy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameSecretIdLookup - errors', () => {
      it('should have a postAuthApproleRoleRoleNameSecretIdLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameSecretIdLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameSecretIdLookup(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameSecretIdLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameTokenBoundCidrs - errors', () => {
      it('should have a getAuthApproleRoleRoleNameTokenBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameTokenBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameTokenBoundCidrs(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameTokenBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameTokenBoundCidrs - errors', () => {
      it('should have a postAuthApproleRoleRoleNameTokenBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameTokenBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameTokenBoundCidrs(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameTokenBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameTokenBoundCidrs - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameTokenBoundCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameTokenBoundCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameTokenBoundCidrs(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameTokenBoundCidrs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameTokenMaxTtl - errors', () => {
      it('should have a getAuthApproleRoleRoleNameTokenMaxTtl function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameTokenMaxTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameTokenMaxTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameTokenMaxTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameTokenMaxTtl - errors', () => {
      it('should have a postAuthApproleRoleRoleNameTokenMaxTtl function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameTokenMaxTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameTokenMaxTtl(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameTokenMaxTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameTokenMaxTtl - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameTokenMaxTtl function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameTokenMaxTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameTokenMaxTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameTokenMaxTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameTokenNumUses - errors', () => {
      it('should have a getAuthApproleRoleRoleNameTokenNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameTokenNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameTokenNumUses(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameTokenNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameTokenNumUses - errors', () => {
      it('should have a postAuthApproleRoleRoleNameTokenNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameTokenNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameTokenNumUses(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameTokenNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameTokenNumUses - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameTokenNumUses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameTokenNumUses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameTokenNumUses(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameTokenNumUses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthApproleRoleRoleNameTokenTtl - errors', () => {
      it('should have a getAuthApproleRoleRoleNameTokenTtl function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthApproleRoleRoleNameTokenTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthApproleRoleRoleNameTokenTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthApproleRoleRoleNameTokenTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleRoleRoleNameTokenTtl - errors', () => {
      it('should have a postAuthApproleRoleRoleNameTokenTtl function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleRoleRoleNameTokenTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthApproleRoleRoleNameTokenTtl(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthApproleRoleRoleNameTokenTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthApproleRoleRoleNameTokenTtl - errors', () => {
      it('should have a deleteAuthApproleRoleRoleNameTokenTtl function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthApproleRoleRoleNameTokenTtl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthApproleRoleRoleNameTokenTtl(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthApproleRoleRoleNameTokenTtl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthApproleTidySecretId - errors', () => {
      it('should have a postAuthApproleTidySecretId function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthApproleTidySecretId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapConfig - errors', () => {
      it('should have a getAuthLdapConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapConfig - errors', () => {
      it('should have a postAuthLdapConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapDuoAccess - errors', () => {
      it('should have a postAuthLdapDuoAccess function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapDuoAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapDuoConfig - errors', () => {
      it('should have a getAuthLdapDuoConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapDuoConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapDuoConfig - errors', () => {
      it('should have a postAuthLdapDuoConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapDuoConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapGroups - errors', () => {
      it('should have a getAuthLdapGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapGroupsName - errors', () => {
      it('should have a getAuthLdapGroupsName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapGroupsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAuthLdapGroupsName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthLdapGroupsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapGroupsName - errors', () => {
      it('should have a postAuthLdapGroupsName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapGroupsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postAuthLdapGroupsName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthLdapGroupsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthLdapGroupsName - errors', () => {
      it('should have a deleteAuthLdapGroupsName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthLdapGroupsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteAuthLdapGroupsName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthLdapGroupsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapLoginUsername - errors', () => {
      it('should have a postAuthLdapLoginUsername function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapLoginUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postAuthLdapLoginUsername(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthLdapLoginUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapMfaConfig - errors', () => {
      it('should have a getAuthLdapMfaConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapMfaConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapMfaConfig - errors', () => {
      it('should have a postAuthLdapMfaConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapMfaConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapUsers - errors', () => {
      it('should have a getAuthLdapUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthLdapUsersName - errors', () => {
      it('should have a getAuthLdapUsersName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthLdapUsersName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAuthLdapUsersName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthLdapUsersName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLdapUsersName - errors', () => {
      it('should have a postAuthLdapUsersName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthLdapUsersName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postAuthLdapUsersName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthLdapUsersName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthLdapUsersName - errors', () => {
      it('should have a deleteAuthLdapUsersName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthLdapUsersName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteAuthLdapUsersName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthLdapUsersName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthRancherConfig - errors', () => {
      it('should have a getAuthRancherConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthRancherConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthRancherConfig - errors', () => {
      it('should have a postAuthRancherConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthRancherConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthRancherLogin - errors', () => {
      it('should have a postAuthRancherLogin function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthRancherLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthRancherRole - errors', () => {
      it('should have a getAuthRancherRole function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthRancherRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthRancherRoleName - errors', () => {
      it('should have a getAuthRancherRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthRancherRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAuthRancherRoleName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthRancherRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthRancherRoleName - errors', () => {
      it('should have a postAuthRancherRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthRancherRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postAuthRancherRoleName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthRancherRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthRancherRoleName - errors', () => {
      it('should have a deleteAuthRancherRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthRancherRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteAuthRancherRoleName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthRancherRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthTokenAccessors - errors', () => {
      it('should have a getAuthTokenAccessors function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthTokenAccessors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenCreate - errors', () => {
      it('should have a postAuthTokenCreate function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenCreateWithNamespaceHeader - errors', () => {
      it('should have a postAuthTokenCreateWithNamespaceHeader function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenCreateWithNamespaceHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenCreateOrphan - errors', () => {
      it('should have a postAuthTokenCreateOrphan function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenCreateOrphan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenCreateRoleName - errors', () => {
      it('should have a postAuthTokenCreateRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenCreateRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthTokenCreateRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthTokenCreateRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthTokenLookup - errors', () => {
      it('should have a getAuthTokenLookup function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthTokenLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenLookup - errors', () => {
      it('should have a postAuthTokenLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenLookupAccessor - errors', () => {
      it('should have a postAuthTokenLookupAccessor function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenLookupAccessor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthTokenLookupSelf - errors', () => {
      it('should have a getAuthTokenLookupSelf function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthTokenLookupSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenLookupSelf - errors', () => {
      it('should have a postAuthTokenLookupSelf function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenLookupSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRenew - errors', () => {
      it('should have a postAuthTokenRenew function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRenew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRenewAccessor - errors', () => {
      it('should have a postAuthTokenRenewAccessor function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRenewAccessor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRenewSelf - errors', () => {
      it('should have a postAuthTokenRenewSelf function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRenewSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRevoke - errors', () => {
      it('should have a postAuthTokenRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRevokeAccessor - errors', () => {
      it('should have a postAuthTokenRevokeAccessor function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRevokeAccessor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRevokeOrphan - errors', () => {
      it('should have a postAuthTokenRevokeOrphan function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRevokeOrphan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRevokeSelf - errors', () => {
      it('should have a postAuthTokenRevokeSelf function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRevokeSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthTokenRoles - errors', () => {
      it('should have a getAuthTokenRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthTokenRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthTokenRolesRoleName - errors', () => {
      it('should have a getAuthTokenRolesRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthTokenRolesRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getAuthTokenRolesRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAuthTokenRolesRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenRolesRoleName - errors', () => {
      it('should have a postAuthTokenRolesRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenRolesRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.postAuthTokenRolesRoleName(null, null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAuthTokenRolesRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthTokenRolesRoleName - errors', () => {
      it('should have a deleteAuthTokenRolesRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthTokenRolesRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteAuthTokenRolesRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAuthTokenRolesRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthTokenTidy - errors', () => {
      it('should have a postAuthTokenTidy function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthTokenTidy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityAlias - errors', () => {
      it('should have a postIdentityAlias function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityAliases - errors', () => {
      it('should have a getIdentityAliases function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityAliasById - errors', () => {
      it('should have a getIdentityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityAliasById - errors', () => {
      it('should have a updateIdentityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityAliasById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityAliasById - errors', () => {
      it('should have a deleteIdentityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityEntity - errors', () => {
      it('should have a postIdentityEntity function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityEntityAlias - errors', () => {
      it('should have a postIdentityEntityAlias function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityEntityAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntityAliases - errors', () => {
      it('should have a getIdentityEntityAliases function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntityAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntityAliasById - errors', () => {
      it('should have a getIdentityEntityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityEntityAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityEntityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityEntityAliasById - errors', () => {
      it('should have a updateIdentityEntityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityEntityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityEntityAliasById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityEntityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityEntityAliasById - errors', () => {
      it('should have a deleteIdentityEntityAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityEntityAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityEntityAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityEntityAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityEntityBatchDelete - errors', () => {
      it('should have a postIdentityEntityBatchDelete function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityEntityBatchDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntities - errors', () => {
      it('should have a getIdentityEntities function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntityById - errors', () => {
      it('should have a getIdentityEntityById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntityById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityEntityById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityEntityById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityEntityById - errors', () => {
      it('should have a updateIdentityEntityById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityEntityById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityEntityById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityEntityById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityEntityById - errors', () => {
      it('should have a deleteIdentityEntityById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityEntityById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityEntityById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityEntityById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityEntityMerge - errors', () => {
      it('should have a postIdentityEntityMerge function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityEntityMerge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntityNames - errors', () => {
      it('should have a getIdentityEntityNames function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntityNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityEntityByName - errors', () => {
      it('should have a getIdentityEntityByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityEntityByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getIdentityEntityByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityEntityByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityEntityByName - errors', () => {
      it('should have a updateIdentityEntityByName function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityEntityByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIdentityEntityByName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityEntityByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityEntityByName - errors', () => {
      it('should have a deleteIdentityEntityByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityEntityByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIdentityEntityByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityEntityByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityGroup - errors', () => {
      it('should have a postIdentityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityGroupAlias - errors', () => {
      it('should have a postIdentityGroupAlias function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityGroupAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroupAliasId - errors', () => {
      it('should have a getIdentityGroupAliasId function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroupAliasId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroupAliasById - errors', () => {
      it('should have a getIdentityGroupAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroupAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityGroupAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityGroupAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityGroupAliasById - errors', () => {
      it('should have a updateIdentityGroupAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityGroupAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityGroupAliasById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityGroupAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityGroupAliasById - errors', () => {
      it('should have a deleteIdentityGroupAliasById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityGroupAliasById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityGroupAliasById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityGroupAliasById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroups - errors', () => {
      it('should have a getIdentityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroupById - errors', () => {
      it('should have a getIdentityGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityGroupById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityGroupById - errors', () => {
      it('should have a updateIdentityGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityGroupById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityGroupById - errors', () => {
      it('should have a deleteIdentityGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityGroupById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroupNames - errors', () => {
      it('should have a getIdentityGroupNames function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroupNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroupByName - errors', () => {
      it('should have a getIdentityGroupByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityGroupByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getIdentityGroupByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityGroupByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityGroupByName - errors', () => {
      it('should have a updateIdentityGroupByName function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityGroupByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIdentityGroupByName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityGroupByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityGroupByName - errors', () => {
      it('should have a deleteIdentityGroupByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityGroupByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIdentityGroupByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityGroupByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityLookupEntity - errors', () => {
      it('should have a postIdentityLookupEntity function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityLookupEntity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityLookupGroup - errors', () => {
      it('should have a postIdentityLookupGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityLookupGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcWellKnownKeys - errors', () => {
      it('should have a getIdentityOidcWellKnownKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcWellKnownKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcWellKnownOpenidConfiguration - errors', () => {
      it('should have a getIdentityOidcWellKnownOpenidConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcWellKnownOpenidConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcConfig - errors', () => {
      it('should have a getIdentityOidcConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityOidcConfig - errors', () => {
      it('should have a postIdentityOidcConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityOidcConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityOidcIntrospect - errors', () => {
      it('should have a postIdentityOidcIntrospect function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityOidcIntrospect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcKeys - errors', () => {
      it('should have a getIdentityOidcKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcKeyByName - errors', () => {
      it('should have a getIdentityOidcKeyByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcKeyByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getIdentityOidcKeyByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityOidcKeyByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityOidcKeyByName - errors', () => {
      it('should have a updateIdentityOidcKeyByName function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityOidcKeyByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIdentityOidcKeyByName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityOidcKeyByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityOidcKeyByName - errors', () => {
      it('should have a deleteIdentityOidcKeyByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityOidcKeyByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIdentityOidcKeyByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityOidcKeyByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityOidcKeyRotateByName - errors', () => {
      it('should have a postIdentityOidcKeyRotateByName function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityOidcKeyRotateByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postIdentityOidcKeyRotateByName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postIdentityOidcKeyRotateByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcRoles - errors', () => {
      it('should have a getIdentityOidcRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcRoleByName - errors', () => {
      it('should have a getIdentityOidcRoleByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcRoleByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getIdentityOidcRoleByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityOidcRoleByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityOidcRoleByName - errors', () => {
      it('should have a updateIdentityOidcRoleByName function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityOidcRoleByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIdentityOidcRoleByName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityOidcRoleByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityOidcRoleByName - errors', () => {
      it('should have a deleteIdentityOidcRoleByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityOidcRoleByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIdentityOidcRoleByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityOidcRoleByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityOidcTokenByName - errors', () => {
      it('should have a getIdentityOidcTokenByName function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityOidcTokenByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getIdentityOidcTokenByName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityOidcTokenByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIdentityPersona - errors', () => {
      it('should have a postIdentityPersona function', (done) => {
        try {
          assert.equal(true, typeof a.postIdentityPersona === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPersonas - errors', () => {
      it('should have a getIdentityPersonas function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityPersonas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityPersonaById - errors', () => {
      it('should have a getIdentityPersonaById function', (done) => {
        try {
          assert.equal(true, typeof a.getIdentityPersonaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIdentityPersonaById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getIdentityPersonaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIdentityPersonaById - errors', () => {
      it('should have a updateIdentityPersonaById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIdentityPersonaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIdentityPersonaById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateIdentityPersonaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIdentityPersonaById - errors', () => {
      it('should have a deleteIdentityPersonaById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIdentityPersonaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIdentityPersonaById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteIdentityPersonaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysAudit - errors', () => {
      it('should have a getSysAudit function', (done) => {
        try {
          assert.equal(true, typeof a.getSysAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysAuditHashPath - errors', () => {
      it('should have a postSysAuditHashPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSysAuditHashPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysAuditHashPath(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysAuditHashPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysAuditPath - errors', () => {
      it('should have a postSysAuditPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSysAuditPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysAuditPath(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysAuditPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysAuditPath - errors', () => {
      it('should have a deleteSysAuditPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysAuditPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteSysAuditPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysAuditPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysAuth - errors', () => {
      it('should have a getSysAuth function', (done) => {
        try {
          assert.equal(true, typeof a.getSysAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysAuthPath - errors', () => {
      it('should have a postSysAuthPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSysAuthPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysAuthPath(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysAuthPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysAuthPath - errors', () => {
      it('should have a deleteSysAuthPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysAuthPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteSysAuthPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysAuthPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysAuthPathTune - errors', () => {
      it('should have a getSysAuthPathTune function', (done) => {
        try {
          assert.equal(true, typeof a.getSysAuthPathTune === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getSysAuthPathTune(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysAuthPathTune', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysAuthPathTune - errors', () => {
      it('should have a postSysAuthPathTune function', (done) => {
        try {
          assert.equal(true, typeof a.postSysAuthPathTune === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysAuthPathTune(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysAuthPathTune', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysCapabilities - errors', () => {
      it('should have a postSysCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.postSysCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysCapabilitiesAccessor - errors', () => {
      it('should have a postSysCapabilitiesAccessor function', (done) => {
        try {
          assert.equal(true, typeof a.postSysCapabilitiesAccessor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysCapabilitiesSelf - errors', () => {
      it('should have a postSysCapabilitiesSelf function', (done) => {
        try {
          assert.equal(true, typeof a.postSysCapabilitiesSelf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigAuditingRequestHeaders - errors', () => {
      it('should have a getSysConfigAuditingRequestHeaders function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigAuditingRequestHeaders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigAuditingRequestHeadersHeader - errors', () => {
      it('should have a getSysConfigAuditingRequestHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigAuditingRequestHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.getSysConfigAuditingRequestHeadersHeader(null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysConfigAuditingRequestHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysConfigAuditingRequestHeadersHeader - errors', () => {
      it('should have a postSysConfigAuditingRequestHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.postSysConfigAuditingRequestHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.postSysConfigAuditingRequestHeadersHeader(null, null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysConfigAuditingRequestHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysConfigAuditingRequestHeadersHeader - errors', () => {
      it('should have a deleteSysConfigAuditingRequestHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysConfigAuditingRequestHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.deleteSysConfigAuditingRequestHeadersHeader(null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysConfigAuditingRequestHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigControlGroup - errors', () => {
      it('should have a getSysConfigControlGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigControlGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysConfigControlGroup - errors', () => {
      it('should have a postSysConfigControlGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postSysConfigControlGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysConfigControlGroup - errors', () => {
      it('should have a deleteSysConfigControlGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysConfigControlGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigCors - errors', () => {
      it('should have a getSysConfigCors function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigCors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysConfigCors - errors', () => {
      it('should have a postSysConfigCors function', (done) => {
        try {
          assert.equal(true, typeof a.postSysConfigCors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysConfigCors - errors', () => {
      it('should have a deleteSysConfigCors function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysConfigCors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigStateSanitized - errors', () => {
      it('should have a getSysConfigStateSanitized function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigStateSanitized === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigUiHeaders - errors', () => {
      it('should have a getSysConfigUiHeaders function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigUiHeaders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysConfigUiHeadersHeader - errors', () => {
      it('should have a getSysConfigUiHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.getSysConfigUiHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.getSysConfigUiHeadersHeader(null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysConfigUiHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysConfigUiHeadersHeader - errors', () => {
      it('should have a postSysConfigUiHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.postSysConfigUiHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.postSysConfigUiHeadersHeader(null, null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysConfigUiHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysConfigUiHeadersHeader - errors', () => {
      it('should have a deleteSysConfigUiHeadersHeader function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysConfigUiHeadersHeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing header', (done) => {
        try {
          a.deleteSysConfigUiHeadersHeader(null, (data, error) => {
            try {
              const displayE = 'header is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysConfigUiHeadersHeader', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysControlGroupAuthorize - errors', () => {
      it('should have a postSysControlGroupAuthorize function', (done) => {
        try {
          assert.equal(true, typeof a.postSysControlGroupAuthorize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysControlGroupRequest - errors', () => {
      it('should have a postSysControlGroupRequest function', (done) => {
        try {
          assert.equal(true, typeof a.postSysControlGroupRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysGenerateRoot - errors', () => {
      it('should have a getSysGenerateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.getSysGenerateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysGenerateRoot - errors', () => {
      it('should have a postSysGenerateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postSysGenerateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysGenerateRoot - errors', () => {
      it('should have a deleteSysGenerateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysGenerateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysGenerateRootAttempt - errors', () => {
      it('should have a getSysGenerateRootAttempt function', (done) => {
        try {
          assert.equal(true, typeof a.getSysGenerateRootAttempt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysGenerateRootAttempt - errors', () => {
      it('should have a postSysGenerateRootAttempt function', (done) => {
        try {
          assert.equal(true, typeof a.postSysGenerateRootAttempt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysGenerateRootAttempt - errors', () => {
      it('should have a deleteSysGenerateRootAttempt function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysGenerateRootAttempt === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysGenerateRootUpdate - errors', () => {
      it('should have a postSysGenerateRootUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postSysGenerateRootUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysHealth - errors', () => {
      it('should have a getSysHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getSysHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysHostInfo - errors', () => {
      it('should have a getSysHostInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getSysHostInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInit - errors', () => {
      it('should have a getSysInit function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysInit - errors', () => {
      it('should have a postSysInit function', (done) => {
        try {
          assert.equal(true, typeof a.postSysInit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalSpecsOpenapi - errors', () => {
      it('should have a getSysInternalSpecsOpenapi function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalSpecsOpenapi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalUiMounts - errors', () => {
      it('should have a getSysInternalUiMounts function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalUiMounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalUiMountsPath - errors', () => {
      it('should have a getSysInternalUiMountsPath function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalUiMountsPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getSysInternalUiMountsPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysInternalUiMountsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysKeyStatus - errors', () => {
      it('should have a getSysKeyStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysKeyStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysLeader - errors', () => {
      it('should have a getSysLeader function', (done) => {
        try {
          assert.equal(true, typeof a.getSysLeader === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesLookup - errors', () => {
      it('should have a postSysLeasesLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysLeasesLookup - errors', () => {
      it('should have a getSysLeasesLookup function', (done) => {
        try {
          assert.equal(true, typeof a.getSysLeasesLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysLeasesLookupPrefix - errors', () => {
      it('should have a getSysLeasesLookupPrefix function', (done) => {
        try {
          assert.equal(true, typeof a.getSysLeasesLookupPrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.getSysLeasesLookupPrefix(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysLeasesLookupPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRenew - errors', () => {
      it('should have a postSysLeasesRenew function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRenew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRenewUrlLeaseId - errors', () => {
      it('should have a postSysLeasesRenewUrlLeaseId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRenewUrlLeaseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlLeaseId', (done) => {
        try {
          a.postSysLeasesRenewUrlLeaseId(null, null, (data, error) => {
            try {
              const displayE = 'urlLeaseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysLeasesRenewUrlLeaseId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRevoke - errors', () => {
      it('should have a postSysLeasesRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRevokeForcePrefix - errors', () => {
      it('should have a postSysLeasesRevokeForcePrefix function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRevokeForcePrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.postSysLeasesRevokeForcePrefix(null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysLeasesRevokeForcePrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRevokePrefixPrefix - errors', () => {
      it('should have a postSysLeasesRevokePrefixPrefix function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRevokePrefixPrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.postSysLeasesRevokePrefixPrefix(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysLeasesRevokePrefixPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesRevokeUrlLeaseId - errors', () => {
      it('should have a postSysLeasesRevokeUrlLeaseId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesRevokeUrlLeaseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlLeaseId', (done) => {
        try {
          a.postSysLeasesRevokeUrlLeaseId(null, null, (data, error) => {
            try {
              const displayE = 'urlLeaseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysLeasesRevokeUrlLeaseId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLeasesTidy - errors', () => {
      it('should have a postSysLeasesTidy function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLeasesTidy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysLicense - errors', () => {
      it('should have a getSysLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getSysLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysLicense - errors', () => {
      it('should have a postSysLicense function', (done) => {
        try {
          assert.equal(true, typeof a.postSysLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMetrics - errors', () => {
      it('should have a getSysMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethod - errors', () => {
      it('should have a getSysMfaMethod function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethodDuoName - errors', () => {
      it('should have a getSysMfaMethodDuoName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethodDuoName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysMfaMethodDuoName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMfaMethodDuoName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodDuoName - errors', () => {
      it('should have a postSysMfaMethodDuoName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodDuoName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodDuoName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodDuoName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysMfaMethodDuoName - errors', () => {
      it('should have a deleteSysMfaMethodDuoName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysMfaMethodDuoName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysMfaMethodDuoName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysMfaMethodDuoName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethodOktaName - errors', () => {
      it('should have a getSysMfaMethodOktaName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethodOktaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysMfaMethodOktaName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMfaMethodOktaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodOktaName - errors', () => {
      it('should have a postSysMfaMethodOktaName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodOktaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodOktaName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodOktaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysMfaMethodOktaName - errors', () => {
      it('should have a deleteSysMfaMethodOktaName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysMfaMethodOktaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysMfaMethodOktaName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysMfaMethodOktaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethodPingidName - errors', () => {
      it('should have a getSysMfaMethodPingidName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethodPingidName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysMfaMethodPingidName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMfaMethodPingidName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodPingidName - errors', () => {
      it('should have a postSysMfaMethodPingidName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodPingidName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodPingidName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodPingidName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysMfaMethodPingidName - errors', () => {
      it('should have a deleteSysMfaMethodPingidName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysMfaMethodPingidName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysMfaMethodPingidName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysMfaMethodPingidName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethodTotpName - errors', () => {
      it('should have a getSysMfaMethodTotpName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethodTotpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysMfaMethodTotpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMfaMethodTotpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodTotpName - errors', () => {
      it('should have a postSysMfaMethodTotpName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodTotpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodTotpName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodTotpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysMfaMethodTotpName - errors', () => {
      it('should have a deleteSysMfaMethodTotpName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysMfaMethodTotpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysMfaMethodTotpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysMfaMethodTotpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodTotpNameAdminDestroy - errors', () => {
      it('should have a postSysMfaMethodTotpNameAdminDestroy function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodTotpNameAdminDestroy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodTotpNameAdminDestroy(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodTotpNameAdminDestroy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMfaMethodTotpNameAdminGenerate - errors', () => {
      it('should have a postSysMfaMethodTotpNameAdminGenerate function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMfaMethodTotpNameAdminGenerate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysMfaMethodTotpNameAdminGenerate(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMfaMethodTotpNameAdminGenerate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMfaMethodTotpNameGenerate - errors', () => {
      it('should have a getSysMfaMethodTotpNameGenerate function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMfaMethodTotpNameGenerate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysMfaMethodTotpNameGenerate(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMfaMethodTotpNameGenerate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMonitor - errors', () => {
      it('should have a getSysMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMounts - errors', () => {
      it('should have a getSysMounts function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMountsPath - errors', () => {
      it('should have a postSysMountsPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMountsPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysMountsPath(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMountsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysMountsPath - errors', () => {
      it('should have a deleteSysMountsPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysMountsPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteSysMountsPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysMountsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysMountsPathTune - errors', () => {
      it('should have a getSysMountsPathTune function', (done) => {
        try {
          assert.equal(true, typeof a.getSysMountsPathTune === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getSysMountsPathTune(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysMountsPathTune', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysMountsPathTune - errors', () => {
      it('should have a postSysMountsPathTune function', (done) => {
        try {
          assert.equal(true, typeof a.postSysMountsPathTune === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysMountsPathTune(null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysMountsPathTune', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysNamespaces - errors', () => {
      it('should have a getSysNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.getSysNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysNamespacesPath - errors', () => {
      it('should have a getSysNamespacesPath function', (done) => {
        try {
          assert.equal(true, typeof a.getSysNamespacesPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getSysNamespacesPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysNamespacesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysNamespacesPath - errors', () => {
      it('should have a postSysNamespacesPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSysNamespacesPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.postSysNamespacesPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysNamespacesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysNamespacesPath - errors', () => {
      it('should have a deleteSysNamespacesPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysNamespacesPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteSysNamespacesPath(null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysNamespacesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPluginsCatalog - errors', () => {
      it('should have a getSysPluginsCatalog function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPluginsCatalog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPluginsCatalogName - errors', () => {
      it('should have a getSysPluginsCatalogName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPluginsCatalogName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPluginsCatalogName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPluginsCatalogName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPluginsCatalogName - errors', () => {
      it('should have a postSysPluginsCatalogName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPluginsCatalogName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPluginsCatalogName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPluginsCatalogName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPluginsCatalogName - errors', () => {
      it('should have a deleteSysPluginsCatalogName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPluginsCatalogName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPluginsCatalogName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPluginsCatalogName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPluginsCatalogType - errors', () => {
      it('should have a getSysPluginsCatalogType function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPluginsCatalogType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getSysPluginsCatalogType(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPluginsCatalogType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPluginsCatalogTypeName - errors', () => {
      it('should have a getSysPluginsCatalogTypeName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPluginsCatalogTypeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPluginsCatalogTypeName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getSysPluginsCatalogTypeName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPluginsCatalogTypeName - errors', () => {
      it('should have a postSysPluginsCatalogTypeName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPluginsCatalogTypeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPluginsCatalogTypeName(null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.postSysPluginsCatalogTypeName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPluginsCatalogTypeName - errors', () => {
      it('should have a deleteSysPluginsCatalogTypeName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPluginsCatalogTypeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPluginsCatalogTypeName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.deleteSysPluginsCatalogTypeName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPluginsCatalogTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPluginsReloadBackend - errors', () => {
      it('should have a postSysPluginsReloadBackend function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPluginsReloadBackend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPluginsReloadBackendStatus - errors', () => {
      it('should have a getSysPluginsReloadBackendStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPluginsReloadBackendStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesAcl - errors', () => {
      it('should have a getSysPoliciesAcl function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesAcl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesAclName - errors', () => {
      it('should have a getSysPoliciesAclName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesAclName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPoliciesAclName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPoliciesAclName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPoliciesAclName - errors', () => {
      it('should have a postSysPoliciesAclName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPoliciesAclName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPoliciesAclName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPoliciesAclName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPoliciesAclName - errors', () => {
      it('should have a deleteSysPoliciesAclName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPoliciesAclName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPoliciesAclName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPoliciesAclName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesEgp - errors', () => {
      it('should have a getSysPoliciesEgp function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesEgp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesEgpName - errors', () => {
      it('should have a getSysPoliciesEgpName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesEgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPoliciesEgpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPoliciesEgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPoliciesEgpName - errors', () => {
      it('should have a postSysPoliciesEgpName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPoliciesEgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPoliciesEgpName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPoliciesEgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPoliciesEgpName - errors', () => {
      it('should have a deleteSysPoliciesEgpName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPoliciesEgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPoliciesEgpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPoliciesEgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesPasswordName - errors', () => {
      it('should have a getSysPoliciesPasswordName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesPasswordName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPoliciesPasswordName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPoliciesPasswordName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPoliciesPasswordName - errors', () => {
      it('should have a postSysPoliciesPasswordName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPoliciesPasswordName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPoliciesPasswordName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPoliciesPasswordName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPoliciesPasswordName - errors', () => {
      it('should have a deleteSysPoliciesPasswordName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPoliciesPasswordName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPoliciesPasswordName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPoliciesPasswordName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesPasswordNameGenerate - errors', () => {
      it('should have a getSysPoliciesPasswordNameGenerate function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesPasswordNameGenerate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPoliciesPasswordNameGenerate(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPoliciesPasswordNameGenerate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesRgp - errors', () => {
      it('should have a getSysPoliciesRgp function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesRgp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPoliciesRgpName - errors', () => {
      it('should have a getSysPoliciesRgpName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPoliciesRgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPoliciesRgpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPoliciesRgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPoliciesRgpName - errors', () => {
      it('should have a postSysPoliciesRgpName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPoliciesRgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPoliciesRgpName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPoliciesRgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPoliciesRgpName - errors', () => {
      it('should have a deleteSysPoliciesRgpName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPoliciesRgpName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPoliciesRgpName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPoliciesRgpName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPolicy - errors', () => {
      it('should have a getSysPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPolicyName - errors', () => {
      it('should have a getSysPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysPolicyName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysPolicyName - errors', () => {
      it('should have a postSysPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysPolicyName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysPolicyName - errors', () => {
      it('should have a deleteSysPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysPolicyName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprof - errors', () => {
      it('should have a getSysPprof function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprof === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofCmdline - errors', () => {
      it('should have a getSysPprofCmdline function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofCmdline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofGoroutine - errors', () => {
      it('should have a getSysPprofGoroutine function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofGoroutine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofHeap - errors', () => {
      it('should have a getSysPprofHeap function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofHeap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofProfile - errors', () => {
      it('should have a getSysPprofProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofSymbol - errors', () => {
      it('should have a getSysPprofSymbol function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofSymbol === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofTrace - errors', () => {
      it('should have a getSysPprofTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysQuotasConfig - errors', () => {
      it('should have a getSysQuotasConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSysQuotasConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysQuotasConfig - errors', () => {
      it('should have a postSysQuotasConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSysQuotasConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysQuotasLeaseCount - errors', () => {
      it('should have a getSysQuotasLeaseCount function', (done) => {
        try {
          assert.equal(true, typeof a.getSysQuotasLeaseCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysQuotasLeaseCountName - errors', () => {
      it('should have a getSysQuotasLeaseCountName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysQuotasLeaseCountName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysQuotasLeaseCountName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysQuotasLeaseCountName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysQuotasLeaseCountName - errors', () => {
      it('should have a postSysQuotasLeaseCountName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysQuotasLeaseCountName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysQuotasLeaseCountName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysQuotasLeaseCountName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysQuotasLeaseCountName - errors', () => {
      it('should have a deleteSysQuotasLeaseCountName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysQuotasLeaseCountName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysQuotasLeaseCountName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysQuotasLeaseCountName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysQuotasRateLimit - errors', () => {
      it('should have a getSysQuotasRateLimit function', (done) => {
        try {
          assert.equal(true, typeof a.getSysQuotasRateLimit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysQuotasRateLimitName - errors', () => {
      it('should have a getSysQuotasRateLimitName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysQuotasRateLimitName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysQuotasRateLimitName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysQuotasRateLimitName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysQuotasRateLimitName - errors', () => {
      it('should have a postSysQuotasRateLimitName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysQuotasRateLimitName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysQuotasRateLimitName(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysQuotasRateLimitName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysQuotasRateLimitName - errors', () => {
      it('should have a deleteSysQuotasRateLimitName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysQuotasRateLimitName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysQuotasRateLimitName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysQuotasRateLimitName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysRekeyBackup - errors', () => {
      it('should have a getSysRekeyBackup function', (done) => {
        try {
          assert.equal(true, typeof a.getSysRekeyBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysRekeyBackup - errors', () => {
      it('should have a deleteSysRekeyBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysRekeyBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysRekeyInit - errors', () => {
      it('should have a getSysRekeyInit function', (done) => {
        try {
          assert.equal(true, typeof a.getSysRekeyInit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRekeyInit - errors', () => {
      it('should have a postSysRekeyInit function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRekeyInit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysRekeyInit - errors', () => {
      it('should have a deleteSysRekeyInit function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysRekeyInit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysRekeyRecoveryKeyBackup - errors', () => {
      it('should have a getSysRekeyRecoveryKeyBackup function', (done) => {
        try {
          assert.equal(true, typeof a.getSysRekeyRecoveryKeyBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysRekeyRecoveryKeyBackup - errors', () => {
      it('should have a deleteSysRekeyRecoveryKeyBackup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysRekeyRecoveryKeyBackup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRekeyUpdate - errors', () => {
      it('should have a postSysRekeyUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRekeyUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysRekeyVerify - errors', () => {
      it('should have a getSysRekeyVerify function', (done) => {
        try {
          assert.equal(true, typeof a.getSysRekeyVerify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRekeyVerify - errors', () => {
      it('should have a postSysRekeyVerify function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRekeyVerify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysRekeyVerify - errors', () => {
      it('should have a deleteSysRekeyVerify function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysRekeyVerify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRemount - errors', () => {
      it('should have a postSysRemount function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRemount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRenew - errors', () => {
      it('should have a postSysRenew function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRenew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRenewUrlLeaseId - errors', () => {
      it('should have a postSysRenewUrlLeaseId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRenewUrlLeaseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlLeaseId', (done) => {
        try {
          a.postSysRenewUrlLeaseId(null, null, (data, error) => {
            try {
              const displayE = 'urlLeaseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysRenewUrlLeaseId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrPrimaryDemote - errors', () => {
      it('should have a postSysReplicationDrPrimaryDemote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrPrimaryDemote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrPrimaryDisable - errors', () => {
      it('should have a postSysReplicationDrPrimaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrPrimaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrPrimaryEnable - errors', () => {
      it('should have a postSysReplicationDrPrimaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrPrimaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrPrimaryRevokeSecondary - errors', () => {
      it('should have a postSysReplicationDrPrimaryRevokeSecondary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrPrimaryRevokeSecondary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrPrimarySecondaryToken - errors', () => {
      it('should have a postSysReplicationDrPrimarySecondaryToken function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrPrimarySecondaryToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryDisable - errors', () => {
      it('should have a postSysReplicationDrSecondaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryEnable - errors', () => {
      it('should have a postSysReplicationDrSecondaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryGeneratePublicKey - errors', () => {
      it('should have a postSysReplicationDrSecondaryGeneratePublicKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryGeneratePublicKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationDrSecondaryLicense - errors', () => {
      it('should have a getSysReplicationDrSecondaryLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationDrSecondaryLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryLicense - errors', () => {
      it('should have a postSysReplicationDrSecondaryLicense function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryOperationTokenDelete - errors', () => {
      it('should have a postSysReplicationDrSecondaryOperationTokenDelete function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryOperationTokenDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryPromote - errors', () => {
      it('should have a postSysReplicationDrSecondaryPromote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryPromote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryRecover - errors', () => {
      it('should have a postSysReplicationDrSecondaryRecover function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryRecover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryReindex - errors', () => {
      it('should have a postSysReplicationDrSecondaryReindex function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryReindex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationDrSecondaryUpdatePrimary - errors', () => {
      it('should have a postSysReplicationDrSecondaryUpdatePrimary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationDrSecondaryUpdatePrimary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationDrStatus - errors', () => {
      it('should have a getSysReplicationDrStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationDrStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryDemote - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryDemote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryDemote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryDisable - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationPerformancePrimaryDynamicFilterId - errors', () => {
      it('should have a getSysReplicationPerformancePrimaryDynamicFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationPerformancePrimaryDynamicFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSysReplicationPerformancePrimaryDynamicFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysReplicationPerformancePrimaryDynamicFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryEnable - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationPerformancePrimaryMountFilterId - errors', () => {
      it('should have a getSysReplicationPerformancePrimaryMountFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationPerformancePrimaryMountFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSysReplicationPerformancePrimaryMountFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysReplicationPerformancePrimaryMountFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryMountFilterId - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryMountFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryMountFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postSysReplicationPerformancePrimaryMountFilterId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysReplicationPerformancePrimaryMountFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysReplicationPerformancePrimaryMountFilterId - errors', () => {
      it('should have a deleteSysReplicationPerformancePrimaryMountFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysReplicationPerformancePrimaryMountFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSysReplicationPerformancePrimaryMountFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysReplicationPerformancePrimaryMountFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationPerformancePrimaryPathsFilterId - errors', () => {
      it('should have a getSysReplicationPerformancePrimaryPathsFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationPerformancePrimaryPathsFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSysReplicationPerformancePrimaryPathsFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysReplicationPerformancePrimaryPathsFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryPathsFilterId - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryPathsFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryPathsFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postSysReplicationPerformancePrimaryPathsFilterId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysReplicationPerformancePrimaryPathsFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysReplicationPerformancePrimaryPathsFilterId - errors', () => {
      it('should have a deleteSysReplicationPerformancePrimaryPathsFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysReplicationPerformancePrimaryPathsFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSysReplicationPerformancePrimaryPathsFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysReplicationPerformancePrimaryPathsFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimaryRevokeSecondary - errors', () => {
      it('should have a postSysReplicationPerformancePrimaryRevokeSecondary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimaryRevokeSecondary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformancePrimarySecondaryToken - errors', () => {
      it('should have a postSysReplicationPerformancePrimarySecondaryToken function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformancePrimarySecondaryToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformanceSecondaryDisable - errors', () => {
      it('should have a postSysReplicationPerformanceSecondaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformanceSecondaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationPerformanceSecondaryDynamicFilterId - errors', () => {
      it('should have a getSysReplicationPerformanceSecondaryDynamicFilterId function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationPerformanceSecondaryDynamicFilterId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSysReplicationPerformanceSecondaryDynamicFilterId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysReplicationPerformanceSecondaryDynamicFilterId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformanceSecondaryEnable - errors', () => {
      it('should have a postSysReplicationPerformanceSecondaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformanceSecondaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformanceSecondaryGeneratePublicKey - errors', () => {
      it('should have a postSysReplicationPerformanceSecondaryGeneratePublicKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformanceSecondaryGeneratePublicKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformanceSecondaryPromote - errors', () => {
      it('should have a postSysReplicationPerformanceSecondaryPromote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformanceSecondaryPromote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPerformanceSecondaryUpdatePrimary - errors', () => {
      it('should have a postSysReplicationPerformanceSecondaryUpdatePrimary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPerformanceSecondaryUpdatePrimary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationPerformanceStatus - errors', () => {
      it('should have a getSysReplicationPerformanceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationPerformanceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPrimaryDemote - errors', () => {
      it('should have a postSysReplicationPrimaryDemote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPrimaryDemote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPrimaryDisable - errors', () => {
      it('should have a postSysReplicationPrimaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPrimaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPrimaryEnable - errors', () => {
      it('should have a postSysReplicationPrimaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPrimaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPrimaryRevokeSecondary - errors', () => {
      it('should have a postSysReplicationPrimaryRevokeSecondary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPrimaryRevokeSecondary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationPrimarySecondaryToken - errors', () => {
      it('should have a postSysReplicationPrimarySecondaryToken function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationPrimarySecondaryToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationRecover - errors', () => {
      it('should have a postSysReplicationRecover function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationRecover === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationReindex - errors', () => {
      it('should have a postSysReplicationReindex function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationReindex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationSecondaryDisable - errors', () => {
      it('should have a postSysReplicationSecondaryDisable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationSecondaryDisable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationSecondaryEnable - errors', () => {
      it('should have a postSysReplicationSecondaryEnable function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationSecondaryEnable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationSecondaryPromote - errors', () => {
      it('should have a postSysReplicationSecondaryPromote function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationSecondaryPromote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysReplicationSecondaryUpdatePrimary - errors', () => {
      it('should have a postSysReplicationSecondaryUpdatePrimary function', (done) => {
        try {
          assert.equal(true, typeof a.postSysReplicationSecondaryUpdatePrimary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysReplicationStatus - errors', () => {
      it('should have a getSysReplicationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysReplicationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRevoke - errors', () => {
      it('should have a postSysRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRevokeForcePrefix - errors', () => {
      it('should have a postSysRevokeForcePrefix function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRevokeForcePrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.postSysRevokeForcePrefix(null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysRevokeForcePrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRevokePrefixPrefix - errors', () => {
      it('should have a postSysRevokePrefixPrefix function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRevokePrefixPrefix === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.postSysRevokePrefixPrefix(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysRevokePrefixPrefix', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRevokeUrlLeaseId - errors', () => {
      it('should have a postSysRevokeUrlLeaseId function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRevokeUrlLeaseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlLeaseId', (done) => {
        try {
          a.postSysRevokeUrlLeaseId(null, null, (data, error) => {
            try {
              const displayE = 'urlLeaseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysRevokeUrlLeaseId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRotate - errors', () => {
      it('should have a postSysRotate function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysSeal - errors', () => {
      it('should have a postSysSeal function', (done) => {
        try {
          assert.equal(true, typeof a.postSysSeal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysSealStatus - errors', () => {
      it('should have a getSysSealStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSysSealStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysSealwrapRewrap - errors', () => {
      it('should have a getSysSealwrapRewrap function', (done) => {
        try {
          assert.equal(true, typeof a.getSysSealwrapRewrap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysSealwrapRewrap - errors', () => {
      it('should have a postSysSealwrapRewrap function', (done) => {
        try {
          assert.equal(true, typeof a.postSysSealwrapRewrap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStepDown - errors', () => {
      it('should have a postSysStepDown function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStepDown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftBootstrapAnswer - errors', () => {
      it('should have a postSysStorageRaftBootstrapAnswer function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftBootstrapAnswer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftBootstrapChallenge - errors', () => {
      it('should have a postSysStorageRaftBootstrapChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftBootstrapChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftConfiguration - errors', () => {
      it('should have a getSysStorageRaftConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftRemovePeer - errors', () => {
      it('should have a postSysStorageRaftRemovePeer function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftRemovePeer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftSnapshot - errors', () => {
      it('should have a getSysStorageRaftSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftSnapshot - errors', () => {
      it('should have a postSysStorageRaftSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftSnapshotForce - errors', () => {
      it('should have a postSysStorageRaftSnapshotForce function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftSnapshotForce === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysToolsHash - errors', () => {
      it('should have a postSysToolsHash function', (done) => {
        try {
          assert.equal(true, typeof a.postSysToolsHash === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysToolsHashUrlalgorithm - errors', () => {
      it('should have a postSysToolsHashUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postSysToolsHashUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postSysToolsHashUrlalgorithm(null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysToolsHashUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysToolsRandom - errors', () => {
      it('should have a postSysToolsRandom function', (done) => {
        try {
          assert.equal(true, typeof a.postSysToolsRandom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysToolsRandomUrlbytes - errors', () => {
      it('should have a postSysToolsRandomUrlbytes function', (done) => {
        try {
          assert.equal(true, typeof a.postSysToolsRandomUrlbytes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlbytes', (done) => {
        try {
          a.postSysToolsRandomUrlbytes(null, null, (data, error) => {
            try {
              const displayE = 'urlbytes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysToolsRandomUrlbytes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysUnseal - errors', () => {
      it('should have a postSysUnseal function', (done) => {
        try {
          assert.equal(true, typeof a.postSysUnseal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysWrappingLookup - errors', () => {
      it('should have a getSysWrappingLookup function', (done) => {
        try {
          assert.equal(true, typeof a.getSysWrappingLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysWrappingLookup - errors', () => {
      it('should have a postSysWrappingLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postSysWrappingLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysWrappingRewrap - errors', () => {
      it('should have a postSysWrappingRewrap function', (done) => {
        try {
          assert.equal(true, typeof a.postSysWrappingRewrap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysWrappingUnwrap - errors', () => {
      it('should have a postSysWrappingUnwrap function', (done) => {
        try {
          assert.equal(true, typeof a.postSysWrappingUnwrap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysWrappingWrap - errors', () => {
      it('should have a postSysWrappingWrap function', (done) => {
        try {
          assert.equal(true, typeof a.postSysWrappingWrap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdConfig - errors', () => {
      it('should have a getAdConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAdConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdConfig - errors', () => {
      it('should have a createAdConfig function', (done) => {
        try {
          assert.equal(true, typeof a.createAdConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdConfig - errors', () => {
      it('should have a deleteAdConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdCredsName - errors', () => {
      it('should have a getAdCredsName function', (done) => {
        try {
          assert.equal(true, typeof a.getAdCredsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAdCredsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAdCredsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdLibrary - errors', () => {
      it('should have a getAdLibrary function', (done) => {
        try {
          assert.equal(true, typeof a.getAdLibrary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdLibraryManageNameCheckIn - errors', () => {
      it('should have a createAdLibraryManageNameCheckIn function', (done) => {
        try {
          assert.equal(true, typeof a.createAdLibraryManageNameCheckIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createAdLibraryManageNameCheckIn('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-createAdLibraryManageNameCheckIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdLibraryName - errors', () => {
      it('should have a getAdLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.getAdLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAdLibraryName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAdLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAdLibraryName - errors', () => {
      it('should have a updateAdLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.updateAdLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateAdLibraryName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateAdLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdLibraryName - errors', () => {
      it('should have a deleteAdLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteAdLibraryName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAdLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdLibraryNameCheckIn - errors', () => {
      it('should have a createAdLibraryNameCheckIn function', (done) => {
        try {
          assert.equal(true, typeof a.createAdLibraryNameCheckIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createAdLibraryNameCheckIn('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-createAdLibraryNameCheckIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdLibraryNameCheckOut - errors', () => {
      it('should have a createAdLibraryNameCheckOut function', (done) => {
        try {
          assert.equal(true, typeof a.createAdLibraryNameCheckOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createAdLibraryNameCheckOut('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-createAdLibraryNameCheckOut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdLibraryNameStatus - errors', () => {
      it('should have a getAdLibraryNameStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getAdLibraryNameStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAdLibraryNameStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAdLibraryNameStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdRotateRoot - errors', () => {
      it('should have a getAdRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.getAdRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdRotateRoot - errors', () => {
      it('should have a createAdRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.createAdRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlicloudRole - errors', () => {
      it('should have a getAlicloudRole function', (done) => {
        try {
          assert.equal(true, typeof a.getAlicloudRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlicloudRoleName - errors', () => {
      it('should have a getAlicloudRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getAlicloudRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAlicloudRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAlicloudRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAlicloudRoleName - errors', () => {
      it('should have a postAlicloudRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postAlicloudRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postAlicloudRoleName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAlicloudRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlicloudRoleName - errors', () => {
      it('should have a deleteAlicloudRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlicloudRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteAlicloudRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteAlicloudRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAwsConfigLease - errors', () => {
      it('should have a getAwsConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.getAwsConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAwsConfigLease - errors', () => {
      it('should have a postAwsConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.postAwsConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAwsConfigRoot - errors', () => {
      it('should have a getAwsConfigRoot function', (done) => {
        try {
          assert.equal(true, typeof a.getAwsConfigRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAwsConfigRoot - errors', () => {
      it('should have a postAwsConfigRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postAwsConfigRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAwsConfigRotateRoot - errors', () => {
      it('should have a postAwsConfigRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postAwsConfigRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAwsCreds - errors', () => {
      it('should have a getAwsCreds function', (done) => {
        try {
          assert.equal(true, typeof a.getAwsCreds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAwsCreds - errors', () => {
      it('should have a postAwsCreds function', (done) => {
        try {
          assert.equal(true, typeof a.postAwsCreds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAwsStsName - errors', () => {
      it('should have a getAwsStsName function', (done) => {
        try {
          assert.equal(true, typeof a.getAwsStsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getAwsStsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAwsStsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAwsStsName - errors', () => {
      it('should have a postAwsStsName function', (done) => {
        try {
          assert.equal(true, typeof a.postAwsStsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postAwsStsName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postAwsStsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAzureCredsRole - errors', () => {
      it('should have a getAzureCredsRole function', (done) => {
        try {
          assert.equal(true, typeof a.getAzureCredsRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getAzureCredsRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getAzureCredsRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsulConfigAccess - errors', () => {
      it('should have a getConsulConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.getConsulConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConsulConfigAccess - errors', () => {
      it('should have a postConsulConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.postConsulConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsulCredsRole - errors', () => {
      it('should have a getConsulCredsRole function', (done) => {
        try {
          assert.equal(true, typeof a.getConsulCredsRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getConsulCredsRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getConsulCredsRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCubbyholePath - errors', () => {
      it('should have a getCubbyholePath function', (done) => {
        try {
          assert.equal(true, typeof a.getCubbyholePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.getCubbyholePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCubbyholePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCubbyholePath - errors', () => {
      it('should have a postCubbyholePath function', (done) => {
        try {
          assert.equal(true, typeof a.postCubbyholePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postCubbyholePath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCubbyholePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCubbyholePath - errors', () => {
      it('should have a deleteCubbyholePath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCubbyholePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.deleteCubbyholePath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteCubbyholePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatabaseConfigName - errors', () => {
      it('should have a getDatabaseConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.getDatabaseConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDatabaseConfigName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getDatabaseConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDatabaseConfigName - errors', () => {
      it('should have a postDatabaseConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.postDatabaseConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postDatabaseConfigName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postDatabaseConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDatabaseConfigName - errors', () => {
      it('should have a deleteDatabaseConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDatabaseConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteDatabaseConfigName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteDatabaseConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDatabaseResetName - errors', () => {
      it('should have a postDatabaseResetName function', (done) => {
        try {
          assert.equal(true, typeof a.postDatabaseResetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postDatabaseResetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postDatabaseResetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDatabaseRotateRoleName - errors', () => {
      it('should have a postDatabaseRotateRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postDatabaseRotateRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postDatabaseRotateRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postDatabaseRotateRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDatabaseRotateRootName - errors', () => {
      it('should have a postDatabaseRotateRootName function', (done) => {
        try {
          assert.equal(true, typeof a.postDatabaseRotateRootName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postDatabaseRotateRootName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postDatabaseRotateRootName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatabaseStaticCredsName - errors', () => {
      it('should have a getDatabaseStaticCredsName function', (done) => {
        try {
          assert.equal(true, typeof a.getDatabaseStaticCredsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDatabaseStaticCredsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getDatabaseStaticCredsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatabaseStaticRoles - errors', () => {
      it('should have a getDatabaseStaticRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getDatabaseStaticRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDatabaseStaticRolesName - errors', () => {
      it('should have a getDatabaseStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.getDatabaseStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getDatabaseStaticRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getDatabaseStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDatabaseStaticRolesName - errors', () => {
      it('should have a postDatabaseStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.postDatabaseStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postDatabaseStaticRolesName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postDatabaseStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDatabaseStaticRolesName - errors', () => {
      it('should have a deleteDatabaseStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDatabaseStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteDatabaseStaticRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteDatabaseStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpConfigRotateRoot - errors', () => {
      it('should have a postGcpConfigRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpConfigRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpKeyRoleset - errors', () => {
      it('should have a getGcpKeyRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpKeyRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.getGcpKeyRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpKeyRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpKeyRoleset - errors', () => {
      it('should have a postGcpKeyRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpKeyRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.postGcpKeyRoleset('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpKeyRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpRolesetName - errors', () => {
      it('should have a getGcpRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getGcpRolesetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpRolesetName - errors', () => {
      it('should have a postGcpRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postGcpRolesetName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpRolesetName - errors', () => {
      it('should have a deleteGcpRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGcpRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteGcpRolesetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteGcpRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpRolesetNameRotate - errors', () => {
      it('should have a postGcpRolesetNameRotate function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpRolesetNameRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postGcpRolesetNameRotate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpRolesetNameRotate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpRolesetNameRotateKey - errors', () => {
      it('should have a postGcpRolesetNameRotateKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpRolesetNameRotateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postGcpRolesetNameRotateKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpRolesetNameRotateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpRolesets - errors', () => {
      it('should have a getGcpRolesets function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpRolesets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpTokenRoleset - errors', () => {
      it('should have a getGcpTokenRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpTokenRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.getGcpTokenRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpTokenRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpTokenRoleset - errors', () => {
      it('should have a postGcpTokenRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpTokenRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.postGcpTokenRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpTokenRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsDecryptKey - errors', () => {
      it('should have a postGcpkmsDecryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsDecryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsDecryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsDecryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsEncryptKey - errors', () => {
      it('should have a postGcpkmsEncryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsEncryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsEncryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsEncryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpkmsKeys - errors', () => {
      it('should have a getGcpkmsKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpkmsKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpkmsKeysConfigKey - errors', () => {
      it('should have a getGcpkmsKeysConfigKey function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpkmsKeysConfigKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getGcpkmsKeysConfigKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpkmsKeysConfigKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysConfigKey - errors', () => {
      it('should have a postGcpkmsKeysConfigKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysConfigKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysConfigKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysConfigKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysDeregisterKey - errors', () => {
      it('should have a postGcpkmsKeysDeregisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysDeregisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysDeregisterKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysDeregisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpkmsKeysDeregisterKey - errors', () => {
      it('should have a deleteGcpkmsKeysDeregisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGcpkmsKeysDeregisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteGcpkmsKeysDeregisterKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteGcpkmsKeysDeregisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysRegisterKey - errors', () => {
      it('should have a postGcpkmsKeysRegisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysRegisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysRegisterKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysRegisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysRotateKey - errors', () => {
      it('should have a postGcpkmsKeysRotateKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysRotateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysRotateKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysRotateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysTrimKey - errors', () => {
      it('should have a postGcpkmsKeysTrimKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysTrimKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysTrimKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysTrimKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpkmsKeysTrimKey - errors', () => {
      it('should have a deleteGcpkmsKeysTrimKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGcpkmsKeysTrimKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteGcpkmsKeysTrimKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteGcpkmsKeysTrimKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpkmsKeysKey - errors', () => {
      it('should have a getGcpkmsKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpkmsKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getGcpkmsKeysKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpkmsKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsKeysKey - errors', () => {
      it('should have a postGcpkmsKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsKeysKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGcpkmsKeysKey - errors', () => {
      it('should have a deleteGcpkmsKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGcpkmsKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteGcpkmsKeysKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteGcpkmsKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGcpkmsPubkeyKey - errors', () => {
      it('should have a getGcpkmsPubkeyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getGcpkmsPubkeyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getGcpkmsPubkeyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getGcpkmsPubkeyKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsReencryptKey - errors', () => {
      it('should have a postGcpkmsReencryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsReencryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsReencryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsReencryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsSignKey - errors', () => {
      it('should have a postGcpkmsSignKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsSignKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsSignKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsSignKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGcpkmsVerifyKey - errors', () => {
      it('should have a postGcpkmsVerifyKey function', (done) => {
        try {
          assert.equal(true, typeof a.postGcpkmsVerifyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postGcpkmsVerifyKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postGcpkmsVerifyKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKvDataPath - errors', () => {
      it('should have a getKvDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getKvDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.getKvDataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getKvDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvDataPath - errors', () => {
      it('should have a postKvDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postKvDataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKvDataPath - errors', () => {
      it('should have a deleteKvDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKvDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.deleteKvDataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteKvDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvDeletePath - errors', () => {
      it('should have a postKvDeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvDeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postKvDeletePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvDeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvDestroyPath - errors', () => {
      it('should have a postKvDestroyPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvDestroyPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postKvDestroyPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvDestroyPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKvMetadataPath - errors', () => {
      it('should have a getKvMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getKvMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.getKvMetadataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getKvMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvMetadataPath - errors', () => {
      it('should have a postKvMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postKvMetadataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKvMetadataPath - errors', () => {
      it('should have a deleteKvMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKvMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.deleteKvMetadataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteKvMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postKvUndeletePath - errors', () => {
      it('should have a postKvUndeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postKvUndeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postKvUndeletePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postKvUndeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNomadConfigAccess - errors', () => {
      it('should have a getNomadConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.getNomadConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNomadConfigAccess - errors', () => {
      it('should have a postNomadConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.postNomadConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNomadConfigAccess - errors', () => {
      it('should have a deleteNomadConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNomadConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNomadConfigLease - errors', () => {
      it('should have a getNomadConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.getNomadConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNomadConfigLease - errors', () => {
      it('should have a postNomadConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.postNomadConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNomadConfigLease - errors', () => {
      it('should have a deleteNomadConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNomadConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNomadRole - errors', () => {
      it('should have a getNomadRole function', (done) => {
        try {
          assert.equal(true, typeof a.getNomadRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNomadRoleName - errors', () => {
      it('should have a getNomadRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getNomadRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getNomadRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getNomadRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNomadRoleName - errors', () => {
      it('should have a postNomadRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postNomadRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postNomadRoleName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postNomadRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNomadRoleName - errors', () => {
      it('should have a deleteNomadRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNomadRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteNomadRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteNomadRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCa - errors', () => {
      it('should have a getPkiCa function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCaPem - errors', () => {
      it('should have a getPkiCaPem function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCaPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCaChain - errors', () => {
      it('should have a getPkiCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCertCaChain - errors', () => {
      it('should have a getPkiCertCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCertCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCertCrl - errors', () => {
      it('should have a getPkiCertCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCertCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCertSerial - errors', () => {
      it('should have a getPkiCertSerial function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCertSerial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getPkiCertSerial('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getPkiCertSerial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCerts - errors', () => {
      it('should have a getPkiCerts function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiConfigCa - errors', () => {
      it('should have a postPkiConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiConfigCrl - errors', () => {
      it('should have a getPkiConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiConfigCrl - errors', () => {
      it('should have a postPkiConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiConfigUrls - errors', () => {
      it('should have a getPkiConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiConfigUrls - errors', () => {
      it('should have a postPkiConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCrl - errors', () => {
      it('should have a getPkiCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCrlPem - errors', () => {
      it('should have a getPkiCrlPem function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCrlPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPkiCrlRotate - errors', () => {
      it('should have a getPkiCrlRotate function', (done) => {
        try {
          assert.equal(true, typeof a.getPkiCrlRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiIntermediateGenerateExported - errors', () => {
      it('should have a postPkiIntermediateGenerateExported function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiIntermediateGenerateExported === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postPkiIntermediateGenerateExported('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiIntermediateGenerateExported', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiIntermediateSetSigned - errors', () => {
      it('should have a postPkiIntermediateSetSigned function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiIntermediateSetSigned === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiIssueRole - errors', () => {
      it('should have a postPkiIssueRole function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiIssueRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiIssueRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiIssueRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiRevoke - errors', () => {
      it('should have a postPkiRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePkiRoot - errors', () => {
      it('should have a deletePkiRoot function', (done) => {
        try {
          assert.equal(true, typeof a.deletePkiRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiRootGenerateExported - errors', () => {
      it('should have a postPkiRootGenerateExported function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiRootGenerateExported === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postPkiRootGenerateExported('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiRootGenerateExported', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiRootSignIntermediate - errors', () => {
      it('should have a postPkiRootSignIntermediate function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiRootSignIntermediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiRootSignSelfIssued - errors', () => {
      it('should have a postPkiRootSignSelfIssued function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiRootSignSelfIssued === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSignVerbatim - errors', () => {
      it('should have a postPkiSignVerbatim function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSignVerbatim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSignVerbatimRole - errors', () => {
      it('should have a postPkiSignVerbatimRole function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSignVerbatimRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiSignVerbatimRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSignVerbatimRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiSignRole - errors', () => {
      it('should have a postPkiSignRole function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiSignRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postPkiSignRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postPkiSignRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPkiTidy - errors', () => {
      it('should have a postPkiTidy function', (done) => {
        try {
          assert.equal(true, typeof a.postPkiTidy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRabbitmqConfigConnection - errors', () => {
      it('should have a postRabbitmqConfigConnection function', (done) => {
        try {
          assert.equal(true, typeof a.postRabbitmqConfigConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRabbitmqConfigLease - errors', () => {
      it('should have a getRabbitmqConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.getRabbitmqConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRabbitmqConfigLease - errors', () => {
      it('should have a postRabbitmqConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.postRabbitmqConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfig - errors', () => {
      it('should have a getSecretEngineConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfig - errors', () => {
      it('should have a postSecretEngineConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineConfig - errors', () => {
      it('should have a deleteSecretEngineConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineCredsName - errors', () => {
      it('should have a getSecretEngineCredsName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineCredsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineCredsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineCredsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineLibrary - errors', () => {
      it('should have a getSecretEngineLibrary function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineLibrary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineLibraryManageNameCheckIn - errors', () => {
      it('should have a postSecretEngineLibraryManageNameCheckIn function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineLibraryManageNameCheckIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineLibraryManageNameCheckIn('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineLibraryManageNameCheckIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineLibraryName - errors', () => {
      it('should have a getSecretEngineLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineLibraryName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecretEngineLibraryName - errors', () => {
      it('should have a updateSecretEngineLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecretEngineLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateSecretEngineLibraryName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-updateSecretEngineLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineLibraryName - errors', () => {
      it('should have a deleteSecretEngineLibraryName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineLibraryName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineLibraryName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineLibraryName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineLibraryNameCheckIn - errors', () => {
      it('should have a postSecretEngineLibraryNameCheckIn function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineLibraryNameCheckIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineLibraryNameCheckIn('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineLibraryNameCheckIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineLibraryNameCheckOut - errors', () => {
      it('should have a postSecretEngineLibraryNameCheckOut function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineLibraryNameCheckOut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineLibraryNameCheckOut('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineLibraryNameCheckOut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineLibraryNameStatus - errors', () => {
      it('should have a getSecretEngineLibraryNameStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineLibraryNameStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineLibraryNameStatus('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineLibraryNameStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRotateRoot - errors', () => {
      it('should have a getSecretEngineRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRotateRoot - errors', () => {
      it('should have a postSecretEngineRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRole - errors', () => {
      it('should have a getSecretEngineRole function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRoleName - errors', () => {
      it('should have a getSecretEngineRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRoleName - errors', () => {
      it('should have a postSecretEngineRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRoleName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineRoleName - errors', () => {
      it('should have a deleteSecretEngineRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfigLease - errors', () => {
      it('should have a getSecretEngineConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigLease - errors', () => {
      it('should have a postSecretEngineConfigLease function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfigRoot - errors', () => {
      it('should have a getSecretEngineConfigRoot function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfigRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigRoot - errors', () => {
      it('should have a postSecretEngineConfigRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigRotateRoot - errors', () => {
      it('should have a postSecretEngineConfigRotateRoot function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigRotateRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineCreds - errors', () => {
      it('should have a getSecretEngineCreds function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineCreds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineCreds - errors', () => {
      it('should have a postSecretEngineCreds function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineCreds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineStsName - errors', () => {
      it('should have a getSecretEngineStsName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineStsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineStsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineStsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineStsName - errors', () => {
      it('should have a postSecretEngineStsName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineStsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineStsName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineStsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineCredsRole - errors', () => {
      it('should have a getSecretEngineCredsRole function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineCredsRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getSecretEngineCredsRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineCredsRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfigAccess - errors', () => {
      it('should have a getSecretEngineConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigAccess - errors', () => {
      it('should have a postSecretEngineConfigAccess function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEnginePath - errors', () => {
      it('should have a getSecretEnginePath function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEnginePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretpath', (done) => {
        try {
          a.getSecretEnginePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'secretpath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEnginePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEnginePath - errors', () => {
      it('should have a postSecretEnginePath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEnginePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretpath', (done) => {
        try {
          a.postSecretEnginePath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretpath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEnginePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEnginePath - errors', () => {
      it('should have a deleteSecretEnginePath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEnginePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secretpath', (done) => {
        try {
          a.deleteSecretEnginePath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'secretpath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEnginePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfigName - errors', () => {
      it('should have a getSecretEngineConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineConfigName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigName - errors', () => {
      it('should have a postSecretEngineConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineConfigName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineConfigName - errors', () => {
      it('should have a deleteSecretEngineConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineConfigName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineResetName - errors', () => {
      it('should have a postSecretEngineResetName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineResetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineResetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineResetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRotateRoleName - errors', () => {
      it('should have a postSecretEngineRotateRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRotateRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRotateRoleName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRotateRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRotateRootName - errors', () => {
      it('should have a postSecretEngineRotateRootName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRotateRootName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRotateRootName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRotateRootName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineStaticCredsName - errors', () => {
      it('should have a getSecretEngineStaticCredsName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineStaticCredsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineStaticCredsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineStaticCredsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineStaticRoles - errors', () => {
      it('should have a getSecretEngineStaticRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineStaticRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineStaticRolesName - errors', () => {
      it('should have a getSecretEngineStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineStaticRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineStaticRolesName - errors', () => {
      it('should have a postSecretEngineStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineStaticRolesName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineStaticRolesName - errors', () => {
      it('should have a deleteSecretEngineStaticRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineStaticRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineStaticRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineStaticRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineKeyRoleset - errors', () => {
      it('should have a getSecretEngineKeyRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineKeyRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.getSecretEngineKeyRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineKeyRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeyRoleset - errors', () => {
      it('should have a postSecretEngineKeyRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeyRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.postSecretEngineKeyRoleset('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeyRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRolesetName - errors', () => {
      it('should have a getSecretEngineRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineRolesetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRolesetName - errors', () => {
      it('should have a postSecretEngineRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRolesetName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineRolesetName - errors', () => {
      it('should have a deleteSecretEngineRolesetName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineRolesetName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineRolesetName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineRolesetName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRolesetNameRotate - errors', () => {
      it('should have a postSecretEngineRolesetNameRotate function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRolesetNameRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRolesetNameRotate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRolesetNameRotate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRolesetNameRotateKey - errors', () => {
      it('should have a postSecretEngineRolesetNameRotateKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRolesetNameRotateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRolesetNameRotateKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRolesetNameRotateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRolesets - errors', () => {
      it('should have a getSecretEngineRolesets function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRolesets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineTokenRoleset - errors', () => {
      it('should have a getSecretEngineTokenRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineTokenRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.getSecretEngineTokenRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineTokenRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineTokenRoleset - errors', () => {
      it('should have a postSecretEngineTokenRoleset function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineTokenRoleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleset', (done) => {
        try {
          a.postSecretEngineTokenRoleset('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'roleset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineTokenRoleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDecryptKey - errors', () => {
      it('should have a postSecretEngineDecryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDecryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineDecryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDecryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineEncryptKey - errors', () => {
      it('should have a postSecretEngineEncryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineEncryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineEncryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineEncryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineKeys - errors', () => {
      it('should have a getSecretEngineKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineKeysConfigKey - errors', () => {
      it('should have a getSecretEngineKeysConfigKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineKeysConfigKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getSecretEngineKeysConfigKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineKeysConfigKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysConfigKey - errors', () => {
      it('should have a postSecretEngineKeysConfigKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysConfigKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysConfigKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysConfigKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysDeregisterKey - errors', () => {
      it('should have a postSecretEngineKeysDeregisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysDeregisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysDeregisterKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysDeregisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineKeysDeregisterKey - errors', () => {
      it('should have a deleteSecretEngineKeysDeregisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineKeysDeregisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteSecretEngineKeysDeregisterKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineKeysDeregisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysRegisterKey - errors', () => {
      it('should have a postSecretEngineKeysRegisterKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysRegisterKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysRegisterKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysRegisterKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysRotateKey - errors', () => {
      it('should have a postSecretEngineKeysRotateKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysRotateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysRotateKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysRotateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysTrimKey - errors', () => {
      it('should have a postSecretEngineKeysTrimKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysTrimKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysTrimKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysTrimKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineKeysTrimKey - errors', () => {
      it('should have a deleteSecretEngineKeysTrimKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineKeysTrimKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteSecretEngineKeysTrimKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineKeysTrimKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineKeysKey - errors', () => {
      it('should have a getSecretEngineKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getSecretEngineKeysKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysKey - errors', () => {
      it('should have a postSecretEngineKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineKeysKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineKeysKey - errors', () => {
      it('should have a deleteSecretEngineKeysKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineKeysKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteSecretEngineKeysKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineKeysKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEnginePubkeyKey - errors', () => {
      it('should have a getSecretEnginePubkeyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEnginePubkeyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getSecretEnginePubkeyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEnginePubkeyKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineReencryptKey - errors', () => {
      it('should have a postSecretEngineReencryptKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineReencryptKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineReencryptKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineReencryptKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineSignKey - errors', () => {
      it('should have a postSecretEngineSignKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineSignKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineSignKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineSignKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineVerifyKey - errors', () => {
      it('should have a postSecretEngineVerifyKey function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineVerifyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.postSecretEngineVerifyKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineVerifyKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineDataPath - errors', () => {
      it('should have a getSecretEngineDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.getSecretEngineDataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDataPath - errors', () => {
      it('should have a postSecretEngineDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postSecretEngineDataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineDataPath - errors', () => {
      it('should have a deleteSecretEngineDataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineDataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.deleteSecretEngineDataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineDataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDeletePath - errors', () => {
      it('should have a postSecretEngineDeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postSecretEngineDeletePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDestroyPath - errors', () => {
      it('should have a postSecretEngineDestroyPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDestroyPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postSecretEngineDestroyPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDestroyPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineMetadataPath - errors', () => {
      it('should have a getSecretEngineMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.getSecretEngineMetadataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineMetadataPath - errors', () => {
      it('should have a postSecretEngineMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postSecretEngineMetadataPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineMetadataPath - errors', () => {
      it('should have a deleteSecretEngineMetadataPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineMetadataPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.deleteSecretEngineMetadataPath('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineMetadataPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineUndeletePath - errors', () => {
      it('should have a postSecretEngineUndeletePath function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineUndeletePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enginepath', (done) => {
        try {
          a.postSecretEngineUndeletePath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enginepath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineUndeletePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigConnection - errors', () => {
      it('should have a postSecretEngineConfigConnection function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineConfigZeroaddress - errors', () => {
      it('should have a getSecretEngineConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineConfigZeroaddress - errors', () => {
      it('should have a postSecretEngineConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineConfigZeroaddress - errors', () => {
      it('should have a deleteSecretEngineConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysKeyName - errors', () => {
      it('should have a postSecretEngineKeysKeyName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysKeyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.postSecretEngineKeysKeyName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysKeyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineKeysKeyName - errors', () => {
      it('should have a deleteSecretEngineKeysKeyName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineKeysKeyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.deleteSecretEngineKeysKeyName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineKeysKeyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineLookup - errors', () => {
      it('should have a postSecretEngineLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEnginePublicKey - errors', () => {
      it('should have a getSecretEnginePublicKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEnginePublicKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineVerify - errors', () => {
      it('should have a postSecretEngineVerify function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineVerify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCa - errors', () => {
      it('should have a getCertCa function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCaPem - errors', () => {
      it('should have a getCertCaPem function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCaPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCaChain - errors', () => {
      it('should have a getCertCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCertCaChain - errors', () => {
      it('should have a getCertCertCaChain function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCertCaChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCertCrl - errors', () => {
      it('should have a getCertCertCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCertCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertSerial - errors', () => {
      it('should have a getCertSerial function', (done) => {
        try {
          assert.equal(true, typeof a.getCertSerial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getCertSerial('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCertSerial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCerts - errors', () => {
      it('should have a getCertCerts function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertConfigCa - errors', () => {
      it('should have a postCertConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.postCertConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertConfigCrl - errors', () => {
      it('should have a getCertConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getCertConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertConfigCrl - errors', () => {
      it('should have a postCertConfigCrl function', (done) => {
        try {
          assert.equal(true, typeof a.postCertConfigCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertConfigUrls - errors', () => {
      it('should have a getCertConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.getCertConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertConfigUrls - errors', () => {
      it('should have a postCertConfigUrls function', (done) => {
        try {
          assert.equal(true, typeof a.postCertConfigUrls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCrl - errors', () => {
      it('should have a getCertCrl function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCrl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCrlPem - errors', () => {
      it('should have a getCertCrlPem function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCrlPem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertCrlRotate - errors', () => {
      it('should have a getCertCrlRotate function', (done) => {
        try {
          assert.equal(true, typeof a.getCertCrlRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertImportQueue - errors', () => {
      it('should have a getCertImportQueue function', (done) => {
        try {
          assert.equal(true, typeof a.getCertImportQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertImportQueueRole - errors', () => {
      it('should have a getCertImportQueueRole function', (done) => {
        try {
          assert.equal(true, typeof a.getCertImportQueueRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getCertImportQueueRole('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCertImportQueueRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertIntermediateGenerateExported - errors', () => {
      it('should have a postCertIntermediateGenerateExported function', (done) => {
        try {
          assert.equal(true, typeof a.postCertIntermediateGenerateExported === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postCertIntermediateGenerateExported('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertIntermediateGenerateExported', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertIntermediateSetSigned - errors', () => {
      it('should have a postCertIntermediateSetSigned function', (done) => {
        try {
          assert.equal(true, typeof a.postCertIntermediateSetSigned === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertIssueRole - errors', () => {
      it('should have a postCertIssueRole function', (done) => {
        try {
          assert.equal(true, typeof a.postCertIssueRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postCertIssueRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertIssueRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertRevoke - errors', () => {
      it('should have a postCertRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.postCertRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRoles - errors', () => {
      it('should have a getSecretEngineRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineRolesName - errors', () => {
      it('should have a getSecretEngineRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRolesName - errors', () => {
      it('should have a postSecretEngineRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRolesName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineRolesName - errors', () => {
      it('should have a deleteSecretEngineRolesName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineRolesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineRolesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineRolesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertRoot - errors', () => {
      it('should have a deleteCertRoot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertRootGenerateExported - errors', () => {
      it('should have a postCertRootGenerateExported function', (done) => {
        try {
          assert.equal(true, typeof a.postCertRootGenerateExported === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exported', (done) => {
        try {
          a.postCertRootGenerateExported('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'exported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertRootGenerateExported', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertRootSignIntermediate - errors', () => {
      it('should have a postCertRootSignIntermediate function', (done) => {
        try {
          assert.equal(true, typeof a.postCertRootSignIntermediate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertRootSignSelfIssued - errors', () => {
      it('should have a postCertRootSignSelfIssued function', (done) => {
        try {
          assert.equal(true, typeof a.postCertRootSignSelfIssued === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertShowVenafiRolePolicyMap - errors', () => {
      it('should have a getCertShowVenafiRolePolicyMap function', (done) => {
        try {
          assert.equal(true, typeof a.getCertShowVenafiRolePolicyMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertSignVerbatim - errors', () => {
      it('should have a postCertSignVerbatim function', (done) => {
        try {
          assert.equal(true, typeof a.postCertSignVerbatim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertSignVerbatimRole - errors', () => {
      it('should have a postCertSignVerbatimRole function', (done) => {
        try {
          assert.equal(true, typeof a.postCertSignVerbatimRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postCertSignVerbatimRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertSignVerbatimRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertSignRole - errors', () => {
      it('should have a postCertSignRole function', (done) => {
        try {
          assert.equal(true, typeof a.postCertSignRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postCertSignRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertSignRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertTidy - errors', () => {
      it('should have a postCertTidy function', (done) => {
        try {
          assert.equal(true, typeof a.postCertTidy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafi - errors', () => {
      it('should have a getCertVenafi function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafiPolicy - errors', () => {
      it('should have a getCertVenafiPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafiPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafiPolicyName - errors', () => {
      it('should have a getCertVenafiPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafiPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getCertVenafiPolicyName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCertVenafiPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertVenafiPolicyName - errors', () => {
      it('should have a postCertVenafiPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.postCertVenafiPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postCertVenafiPolicyName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertVenafiPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertVenafiPolicyName - errors', () => {
      it('should have a deleteCertVenafiPolicyName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertVenafiPolicyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteCertVenafiPolicyName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteCertVenafiPolicyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafiPolicyNamePolicy - errors', () => {
      it('should have a getCertVenafiPolicyNamePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafiPolicyNamePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getCertVenafiPolicyNamePolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCertVenafiPolicyNamePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertVenafiPolicyNamePolicy - errors', () => {
      it('should have a postCertVenafiPolicyNamePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postCertVenafiPolicyNamePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postCertVenafiPolicyNamePolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertVenafiPolicyNamePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafiSyncPolicies - errors', () => {
      it('should have a getCertVenafiSyncPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafiSyncPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertVenafiName - errors', () => {
      it('should have a getCertVenafiName function', (done) => {
        try {
          assert.equal(true, typeof a.getCertVenafiName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getCertVenafiName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getCertVenafiName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCertVenafiName - errors', () => {
      it('should have a postCertVenafiName function', (done) => {
        try {
          assert.equal(true, typeof a.postCertVenafiName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postCertVenafiName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postCertVenafiName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertVenafiName - errors', () => {
      it('should have a deleteCertVenafiName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertVenafiName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteCertVenafiName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteCertVenafiName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineCodeName - errors', () => {
      it('should have a getSecretEngineCodeName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineCodeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineCodeName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineCodeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineCodeName - errors', () => {
      it('should have a postSecretEngineCodeName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineCodeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineCodeName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineCodeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineKeysName - errors', () => {
      it('should have a getSecretEngineKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysName - errors', () => {
      it('should have a postSecretEngineKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineKeysName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretEngineKeysName - errors', () => {
      it('should have a deleteSecretEngineKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecretEngineKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSecretEngineKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSecretEngineKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineBackupName - errors', () => {
      it('should have a getSecretEngineBackupName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineBackupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineBackupName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineBackupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineCacheConfig - errors', () => {
      it('should have a getSecretEngineCacheConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineCacheConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineCacheConfig - errors', () => {
      it('should have a postSecretEngineCacheConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineCacheConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDatakeyPlaintextName - errors', () => {
      it('should have a postSecretEngineDatakeyPlaintextName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDatakeyPlaintextName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineDatakeyPlaintextName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDatakeyPlaintextName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plaintext', (done) => {
        try {
          a.postSecretEngineDatakeyPlaintextName('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'plaintext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDatakeyPlaintextName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineDecryptName - errors', () => {
      it('should have a postSecretEngineDecryptName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineDecryptName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineDecryptName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineDecryptName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineEncryptName - errors', () => {
      it('should have a postSecretEngineEncryptName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineEncryptName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineEncryptName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineEncryptName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineExportTypeName - errors', () => {
      it('should have a getSecretEngineExportTypeName function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineExportTypeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineExportTypeName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineExportTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getSecretEngineExportTypeName('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineExportTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretEngineExportTypeNameVersion - errors', () => {
      it('should have a getSecretEngineExportTypeNameVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getSecretEngineExportTypeNameVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSecretEngineExportTypeNameVersion('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getSecretEngineExportTypeNameVersion('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getSecretEngineExportTypeNameVersion('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSecretEngineExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineHash - errors', () => {
      it('should have a postSecretEngineHash function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineHash === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineHashUrlalgorithm - errors', () => {
      it('should have a postSecretEngineHashUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineHashUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postSecretEngineHashUrlalgorithm('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineHashUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineHmacName - errors', () => {
      it('should have a postSecretEngineHmacName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineHmacName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineHmacName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineHmacName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineHmacNameUrlalgorithm - errors', () => {
      it('should have a postSecretEngineHmacNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineHmacNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineHmacNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineHmacNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postSecretEngineHmacNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineHmacNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysNameConfig - errors', () => {
      it('should have a postSecretEngineKeysNameConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysNameConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineKeysNameConfig('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysNameConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysNameRotate - errors', () => {
      it('should have a postSecretEngineKeysNameRotate function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysNameRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineKeysNameRotate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysNameRotate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineKeysNameTrim - errors', () => {
      it('should have a postSecretEngineKeysNameTrim function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineKeysNameTrim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineKeysNameTrim('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineKeysNameTrim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRandom - errors', () => {
      it('should have a postSecretEngineRandom function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRandom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRandomUrlbytes - errors', () => {
      it('should have a postSecretEngineRandomUrlbytes function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRandomUrlbytes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlbytes', (done) => {
        try {
          a.postSecretEngineRandomUrlbytes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlbytes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRandomUrlbytes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRestore - errors', () => {
      it('should have a postSecretEngineRestore function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRestore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRestoreName - errors', () => {
      it('should have a postSecretEngineRestoreName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRestoreName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRestoreName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRestoreName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineRewrapName - errors', () => {
      it('should have a postSecretEngineRewrapName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineRewrapName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineRewrapName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineRewrapName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineSignName - errors', () => {
      it('should have a postSecretEngineSignName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineSignName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineSignName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineSignName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineSignNameUrlalgorithm - errors', () => {
      it('should have a postSecretEngineSignNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineSignNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineSignNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineSignNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postSecretEngineSignNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineSignNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineVerifyName - errors', () => {
      it('should have a postSecretEngineVerifyName function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineVerifyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineVerifyName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineVerifyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretEngineVerifyNameUrlalgorithm - errors', () => {
      it('should have a postSecretEngineVerifyNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postSecretEngineVerifyNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSecretEngineVerifyNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineVerifyNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postSecretEngineVerifyNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSecretEngineVerifyNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshConfigCa - errors', () => {
      it('should have a getSshConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.getSshConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshConfigCa - errors', () => {
      it('should have a postSshConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.postSshConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshConfigCa - errors', () => {
      it('should have a deleteSshConfigCa function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSshConfigCa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshConfigZeroaddress - errors', () => {
      it('should have a getSshConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.getSshConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshConfigZeroaddress - errors', () => {
      it('should have a postSshConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.postSshConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshConfigZeroaddress - errors', () => {
      it('should have a deleteSshConfigZeroaddress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSshConfigZeroaddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshCredsRole - errors', () => {
      it('should have a postSshCredsRole function', (done) => {
        try {
          assert.equal(true, typeof a.postSshCredsRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postSshCredsRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSshCredsRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshKeysKeyName - errors', () => {
      it('should have a postSshKeysKeyName function', (done) => {
        try {
          assert.equal(true, typeof a.postSshKeysKeyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.postSshKeysKeyName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSshKeysKeyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSshKeysKeyName - errors', () => {
      it('should have a deleteSshKeysKeyName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSshKeysKeyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.deleteSshKeysKeyName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSshKeysKeyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshLookup - errors', () => {
      it('should have a postSshLookup function', (done) => {
        try {
          assert.equal(true, typeof a.postSshLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSshPublicKey - errors', () => {
      it('should have a getSshPublicKey function', (done) => {
        try {
          assert.equal(true, typeof a.getSshPublicKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshSignRole - errors', () => {
      it('should have a postSshSignRole function', (done) => {
        try {
          assert.equal(true, typeof a.postSshSignRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.postSshSignRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSshSignRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSshVerify - errors', () => {
      it('should have a postSshVerify function', (done) => {
        try {
          assert.equal(true, typeof a.postSshVerify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTotpCodeName - errors', () => {
      it('should have a getTotpCodeName function', (done) => {
        try {
          assert.equal(true, typeof a.getTotpCodeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTotpCodeName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTotpCodeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTotpCodeName - errors', () => {
      it('should have a postTotpCodeName function', (done) => {
        try {
          assert.equal(true, typeof a.postTotpCodeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTotpCodeName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTotpCodeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTotpKeys - errors', () => {
      it('should have a getTotpKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getTotpKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTotpKeysName - errors', () => {
      it('should have a getTotpKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.getTotpKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTotpKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTotpKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTotpKeysName - errors', () => {
      it('should have a postTotpKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.postTotpKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTotpKeysName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTotpKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTotpKeysName - errors', () => {
      it('should have a deleteTotpKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTotpKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteTotpKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteTotpKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitBackupName - errors', () => {
      it('should have a getTransitBackupName function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitBackupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTransitBackupName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitBackupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitCacheConfig - errors', () => {
      it('should have a getTransitCacheConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitCacheConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitCacheConfig - errors', () => {
      it('should have a postTransitCacheConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitCacheConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitDatakeyPlaintextName - errors', () => {
      it('should have a postTransitDatakeyPlaintextName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitDatakeyPlaintextName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitDatakeyPlaintextName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitDatakeyPlaintextName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing plaintext', (done) => {
        try {
          a.postTransitDatakeyPlaintextName('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'plaintext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitDatakeyPlaintextName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitDecryptName - errors', () => {
      it('should have a postTransitDecryptName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitDecryptName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitDecryptName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitDecryptName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitEncryptName - errors', () => {
      it('should have a postTransitEncryptName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitEncryptName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitEncryptName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitEncryptName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitExportTypeName - errors', () => {
      it('should have a getTransitExportTypeName function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitExportTypeName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTransitExportTypeName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitExportTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getTransitExportTypeName('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitExportTypeName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitExportTypeNameVersion - errors', () => {
      it('should have a getTransitExportTypeNameVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitExportTypeNameVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTransitExportTypeNameVersion('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getTransitExportTypeNameVersion('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.getTransitExportTypeNameVersion('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitExportTypeNameVersion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitHash - errors', () => {
      it('should have a postTransitHash function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitHash === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitHashUrlalgorithm - errors', () => {
      it('should have a postTransitHashUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitHashUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postTransitHashUrlalgorithm('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitHashUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitHmacName - errors', () => {
      it('should have a postTransitHmacName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitHmacName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitHmacName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitHmacName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitHmacNameUrlalgorithm - errors', () => {
      it('should have a postTransitHmacNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitHmacNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitHmacNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitHmacNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postTransitHmacNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitHmacNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitKeys - errors', () => {
      it('should have a getTransitKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitKeysName - errors', () => {
      it('should have a getTransitKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTransitKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getTransitKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitKeysName - errors', () => {
      it('should have a postTransitKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitKeysName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitKeysName - errors', () => {
      it('should have a deleteTransitKeysName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitKeysName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteTransitKeysName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteTransitKeysName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitKeysNameConfig - errors', () => {
      it('should have a postTransitKeysNameConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitKeysNameConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitKeysNameConfig('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitKeysNameConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitKeysNameRotate - errors', () => {
      it('should have a postTransitKeysNameRotate function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitKeysNameRotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitKeysNameRotate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitKeysNameRotate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitKeysNameTrim - errors', () => {
      it('should have a postTransitKeysNameTrim function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitKeysNameTrim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitKeysNameTrim('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitKeysNameTrim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitRandom - errors', () => {
      it('should have a postTransitRandom function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitRandom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitRandomUrlbytes - errors', () => {
      it('should have a postTransitRandomUrlbytes function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitRandomUrlbytes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlbytes', (done) => {
        try {
          a.postTransitRandomUrlbytes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlbytes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitRandomUrlbytes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitRestore - errors', () => {
      it('should have a postTransitRestore function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitRestore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitRestoreName - errors', () => {
      it('should have a postTransitRestoreName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitRestoreName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitRestoreName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitRestoreName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitRewrapName - errors', () => {
      it('should have a postTransitRewrapName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitRewrapName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitRewrapName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitRewrapName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitSignName - errors', () => {
      it('should have a postTransitSignName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitSignName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitSignName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitSignName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitSignNameUrlalgorithm - errors', () => {
      it('should have a postTransitSignNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitSignNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitSignNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitSignNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postTransitSignNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitSignNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitVerifyName - errors', () => {
      it('should have a postTransitVerifyName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitVerifyName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitVerifyName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitVerifyName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransitVerifyNameUrlalgorithm - errors', () => {
      it('should have a postTransitVerifyNameUrlalgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.postTransitVerifyNameUrlalgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postTransitVerifyNameUrlalgorithm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitVerifyNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlalgorithm', (done) => {
        try {
          a.postTransitVerifyNameUrlalgorithm('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'urlalgorithm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postTransitVerifyNameUrlalgorithm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalCountersActivity - errors', () => {
      it('should have a getSysInternalCountersActivity function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalCountersActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalCountersActivityMonthly - errors', () => {
      it('should have a getSysInternalCountersActivityMonthly function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalCountersActivityMonthly === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalCountersConfig - errors', () => {
      it('should have a getSysInternalCountersConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalCountersConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysInternalCountersConfig - errors', () => {
      it('should have a postSysInternalCountersConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSysInternalCountersConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysInternalUiFeatureFlags - errors', () => {
      it('should have a getSysInternalUiFeatureFlags function', (done) => {
        try {
          assert.equal(true, typeof a.getSysInternalUiFeatureFlags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofAllocs - errors', () => {
      it('should have a getSysPprofAllocs function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofAllocs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofBlock - errors', () => {
      it('should have a getSysPprofBlock function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofMutex - errors', () => {
      it('should have a getSysPprofMutex function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofMutex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysPprofThreadcreate - errors', () => {
      it('should have a getSysPprofThreadcreate function', (done) => {
        try {
          assert.equal(true, typeof a.getSysPprofThreadcreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysRotateConfig - errors', () => {
      it('should have a getSysRotateConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSysRotateConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysRotateConfig - errors', () => {
      it('should have a postSysRotateConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postSysRotateConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftAutopilotConfiguration - errors', () => {
      it('should have a getSysStorageRaftAutopilotConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftAutopilotConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftAutopilotConfiguration - errors', () => {
      it('should have a postSysStorageRaftAutopilotConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftAutopilotConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftAutopilotState - errors', () => {
      it('should have a getSysStorageRaftAutopilotState function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftAutopilotState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftSnapshotAutoConfig - errors', () => {
      it('should have a getSysStorageRaftSnapshotAutoConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftSnapshotAutoConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftSnapshotAutoConfigName - errors', () => {
      it('should have a getSysStorageRaftSnapshotAutoConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftSnapshotAutoConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysStorageRaftSnapshotAutoConfigName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysStorageRaftSnapshotAutoConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSysStorageRaftSnapshotAutoConfigName - errors', () => {
      it('should have a postSysStorageRaftSnapshotAutoConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.postSysStorageRaftSnapshotAutoConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.postSysStorageRaftSnapshotAutoConfigName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-postSysStorageRaftSnapshotAutoConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSysStorageRaftSnapshotAutoConfigName - errors', () => {
      it('should have a deleteSysStorageRaftSnapshotAutoConfigName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSysStorageRaftSnapshotAutoConfigName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteSysStorageRaftSnapshotAutoConfigName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-deleteSysStorageRaftSnapshotAutoConfigName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSysStorageRaftSnapshotAutoStatusName - errors', () => {
      it('should have a getSysStorageRaftSnapshotAutoStatusName function', (done) => {
        try {
          assert.equal(true, typeof a.getSysStorageRaftSnapshotAutoStatusName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSysStorageRaftSnapshotAutoStatusName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-hashicorp_vault-adapter-getSysStorageRaftSnapshotAutoStatusName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
