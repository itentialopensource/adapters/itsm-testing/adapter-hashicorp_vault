
## 0.6.4 [06-20-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!10

---

## 0.6.3 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!10

---

## 0.6.2 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!10

---

## 0.6.1 [01-30-2023]

* Add namespace header to postAuthTokenCreate task

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!8

---

## 0.6.0 [07-22-2022]

* Add new secret calls

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!7

---

## 0.5.0 [06-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!6

---

## 0.4.0 [05-19-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!6

---

## 0.3.2 [03-12-2022]

- Added auth.md file detailing how to authenticate adapter
- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!5

---

## 0.3.1 [03-11-2022]

- Updated healthcheck url and changed auth method to static token

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!4

---

## 0.3.0 [10-07-2021]

- Update the identity calls with namespace

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!2

---

## 0.2.0 [10-06-2021]

The Secret calls in the adapter are hardcoded with particular secret mount points. This needs to be made variables. Can have a default value in case no mount point is provided.

In addition:
- The namespace should also be a variable in the path (can come from workflow or adapter property). If missing nothing is added to the path.
- The method names should reflect what it is without the specific mount pointThe descriptions should be fixed
- The list should be passed properly

See merge request itentialopensource/adapters/itsm-testing/adapter-hashicorp_vault!1

---

## 0.1.1 [08-20-2021]

- Initial Commit

See commit 09dac7d

---
